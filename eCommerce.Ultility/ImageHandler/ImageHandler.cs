﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using ImageResizer;

namespace eCommerce.Ultility
{
    public class ImageHandler
    {
        static Dictionary<string, string> photoFormat = new Dictionary<string, string>
        {
            {"represent_large",     "width=268&height=249&format=jpg"},
            {"represent_small",     "width=110&height=110&format=jpg&crop=auto"},
            {"represent_landscape", "width=536&height=268&format=jpg&crop=auto"},
            {"slide_main",          "width=330&height=380&format=jpg&crop=auto"},
            {"slide_thumbnail",     "width=85&height=85&format=jpg&crop=auto"}
        };

        static Dictionary<string, string> photoPath = new Dictionary<string, string>
        {
            {"represent_large", "~/Images/Product/Represent/Large/"},
            {"represent_small", "~/Images/Product/Represent/Small/"},
            {"represent_landscape", "~/Images/Product/Represent/Landscape/"},
            {"slide_main", "~/Images/Product/Slide/Main/"},
            {"slide_thumbnail", "~/Images/Product/Slide/Thumbnail/"}
        };

        public static void SavePhoto(HttpPostedFileBase file, string fileName, string option)
        {
            file.InputStream.Seek(0, SeekOrigin.Begin);
            ImageBuilder.Current.Build(
                new ImageJob(
                    file.InputStream,
                    photoPath[option] + fileName,
                    new Instructions(photoFormat[option]),
                    false,
                    true
                )
            );
        }
        public static string SavePhoto1(HttpPostedFileBase file, Guid id, string option, int postfix = 0)
        {
            var instructions = "";
            switch (option)
            {
                case "Index":
                    instructions += "width=268&height=249&format=jpg";
                    break;
                case "Detail":
                    instructions += "width=330&height=380&format=jpg";
                    break;
                case "Thumbnail":
                    instructions += "width=85&height=85&format=jpg";
                    break;
            }

            var fileName = new StringBuilder();
            fileName.AppendFormat("{0}_{1}_{2}", id, option, postfix);

            var filePath = new StringBuilder();
            filePath.Append("~/Images/Product/");
            filePath.Append(option + "/");
            filePath.Append(fileName.ToString());

            file.InputStream.Seek(0, SeekOrigin.Begin);
            ImageBuilder.Current.Build(
                new ImageJob(
                    file.InputStream,
                    filePath.ToString(),
                    new Instructions(instructions),
                    false,
                    true
                )
            );
            return fileName.ToString();
        }
    }
}
