﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class OrderDTO : BaseDTO<Guid>
    {
        public decimal Total { get; set; }
        public int Quantity { get; set; }
        public OrderState State { get; set; }

        public Guid UserId { get; set; }

        public virtual IList<OrderDetailDTO> OrderDetails { get; set; }
        public virtual UserDTO User { get; set; }
    }
}
