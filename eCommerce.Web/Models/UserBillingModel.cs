﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class UserBillingModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "The phone number field is invalid")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
    }
}