﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using eCommerce.Domain.IService;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Service.Service;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;

namespace eCommerce.UnitTest.ServiceTest
{
    class RoleServiceTests
    {
        private Mock<IRoleRepository> _repo;
        private IRoleService _service;        
        private readonly List<Role> _roles = new List<Role>();

        [SetUp]
        public void Setup()
        {
            _repo = new Mock<IRoleRepository>();            
            _service = new RoleService(_repo.Object);
            AutoMappingRegistration.Register();

            _roles.Add(new Role() { Id = 1, Name = "User" });
            _roles.Add(new Role() { Id = 2, Name = "Admin" });
        }

        [TestCase("User")]
        public void Get_Role_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<Role, bool>>>())).Returns(_roles.First());

            var result = _service.GetRoleByName(name);

            Assert.IsNotNull(result);
            Assert.AreEqual(name, result.Name);
        }

        [TestCase("Admin")]
        public void Unable_To_Get_Role_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<Role, bool>>>())).Returns((Role)null);

            var result = _service.GetRoleByName(name);

            Assert.IsNull(result);
        }

        [TestCase(2)]
        public void Get_Role_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns(_roles[1]);

            var result = _service.GetRoleById(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.Id);
        }

        [TestCase(1)]
        public void Unable_To_Get_Role_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns((Role)null);

            var result = _service.GetRoleById(id);

            Assert.IsNull(result);
        }

        [Test]
        public void Get_All_Roles()
        {
            _repo.Setup(r => r.GetAll()).Returns(_roles);

            var result = _service.GetAllRoles();

            Assert.IsNotNull(result);
            Assert.AreEqual(_roles.Count, result.Count());
        }
    }
}
