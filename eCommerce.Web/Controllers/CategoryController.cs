﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.Web.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;
        
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Authorize]
        // GET: Category
        public ActionResult Index()
        {
            var categoryDto = _categoryService.GetAllCategory().ToList();
            var categoryModel = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(categoryDto);
            return View(categoryModel);
        }
    }
}