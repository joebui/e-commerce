﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
                
        public UserDTO CheckRegisterUsernameEmail(string name, string email)
        {
            var result = _userRepository.Get(u => u.UserName.Equals(name) || u.Email.Equals(email));
            return Mapper.Map<User, UserDTO>(result);
        }

        public UserDTO CheckLoginUsernamEmail(string nameEmail)
        {
            var result = _userRepository.Get(u => u.UserName.Equals(nameEmail) || u.Email.Equals(nameEmail));
            return Mapper.Map<User, UserDTO>(result);
        }

        public UserDTO FindUserByName(string name)
        {
            var user = _userRepository.Get(u => u.UserName.Equals(name));
            return Mapper.Map<User, UserDTO>(user);
        }

        public UserDTO FindUserById(Guid id)
        {
            var user = _userRepository.GetById(id);
            return Mapper.Map<User, UserDTO>(user);
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = _userRepository.GetMany(u => !u.IsDeleted);
            return users.Select(user => Mapper.Map<User, UserDTO>(user)).ToList();
        }

        public IEnumerable<UserDTO> GetUsersContainsName(string name)
        {
            var users = _userRepository.GetMany(u => u.UserName.Contains(name) && !u.IsDeleted);
            return Mapper.Map<List<User>, List<UserDTO>>(users.ToList());
        }

        public IEnumerable<UserDTO> GetUsersWithRole(string role)
        {
            var users = _userRepository.GetMany(u => u.Role.Name == role && !u.IsDeleted);
            return Mapper.Map<List<User>, List<UserDTO>>(users.ToList());
        }

        public void AddNewUser(UserDTO userDto)
        {
            _userRepository.Add(Mapper.Map<UserDTO, User>(userDto));     
            _unitOfWork.Commit();       
        }

        public void UpdateUser(UserDTO userDto)
        {
            _userRepository.AddOrUpdate(Mapper.Map<UserDTO, User>(userDto));
            _unitOfWork.Commit();
        }

        public void RemoveUser(UserDTO userDto)
        {
            userDto.IsDeleted = true;

            _userRepository.AddOrUpdate(Mapper.Map<UserDTO, User>(userDto));
            _unitOfWork.Commit();
        }
    }
}
