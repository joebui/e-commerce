namespace eCommerce.Framework.Migrations
{
    using Schema.Entity;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Ultility.PasswordHandler;
    using Ultility.Type;
    internal sealed class Configuration : DbMigrationsConfiguration<eCommerce.Framework.DataAccess.ECommerceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(eCommerce.Framework.DataAccess.ECommerceContext context)
        {
            //  These methods will be called after migrating to the latest version.            
            context.Roles.AddOrUpdate
            (
                new Role() { Id = 1, Name = "User" },
                new Role() { Id = 2, Name = "Admin" }
            );

            context.Users.AddOrUpdate
            (
                new User()
                {
                    Id = Guid.Parse("0E984725-C51C-4BF4-9960-E1C80E27ABA0"),
                    UserName = "admin",
                    Email = "admin@mail.com",
                    Address = "default",
                    PhoneNumber = "1234",
                    RoleId = 2,
                    Password = Encryption.Encrypt("admin1234")
                }
            );

            context.Categories.AddOrUpdate(
                new Category { Id = 1, Name = "Clothing & Accessories", UrlSlug = "clothing-and-accessories" },
                new Category { Id = 2, Name = "Jewelry", UrlSlug = "jewelry" },
                new Category { Id = 3, Name = "Craft Supplies & Tools", UrlSlug = "craft-supplies-and-tools" },
                new Category { Id = 4, Name = "Entertainment", UrlSlug = "entertainment" },
                new Category { Id = 5, Name = "Home & Living", UrlSlug = "home-and-living" },
                new Category { Id = 6, Name = "Handmade", UrlSlug = "handmade" }
            );

            context.SubCategories.AddOrUpdate(
                new SubCategory { Id = 1, Name = "Men's Clothing", UrlSlug = "men-s-clothing", CategoryId = 1 },
                new SubCategory { Id = 2, Name = "Women's Clothing", UrlSlug = "women-s-clothing", CategoryId = 1 },
                new SubCategory { Id = 3, Name = "Shoes", UrlSlug = "shoes", CategoryId = 1 },
                new SubCategory { Id = 4, Name = "Bag & Purses", UrlSlug = "bag-and-purses", CategoryId = 1 },
                new SubCategory { Id = 5, Name = "Belts & Suspenders", UrlSlug = "belts-and-suspenders", CategoryId = 1 },
                new SubCategory { Id = 6, Name = "Earings", UrlSlug = "earings", CategoryId = 2 },
                new SubCategory { Id = 7, Name = "Bracelets", UrlSlug = "bracelets", CategoryId = 2 },
                new SubCategory { Id = 8, Name = "Necklaces", UrlSlug = "necklaces", CategoryId = 2 },
                new SubCategory { Id = 9, Name = "Fiber & Textile Art", UrlSlug = "fiber-and-textile-art", CategoryId = 3 },
                new SubCategory { Id = 10, Name = "Food Craft Supplies", UrlSlug = "food-craft-supplies", CategoryId = 3 },
                new SubCategory { Id = 11, Name = "Patterns & Tutorials", UrlSlug = "patterns-and-tutorials", CategoryId = 3 },
                new SubCategory { Id = 12, Name = "Sewing & Needlecraft", UrlSlug = "sewing-and-needlecraft", CategoryId = 3 },
                new SubCategory { Id = 13, Name = "Books", UrlSlug = "books", CategoryId = 4 },
                new SubCategory { Id = 14, Name = "Movies", UrlSlug = "movies", CategoryId = 4 },
                new SubCategory { Id = 15, Name = "Musics", UrlSlug = "musics", CategoryId = 4 },
                new SubCategory { Id = 16, Name = "Games", UrlSlug = "games", CategoryId = 4 },
                new SubCategory { Id = 17, Name = "Art & Collectibles", UrlSlug = "art-and-collectibles", CategoryId = 5 },
                new SubCategory { Id = 18, Name = "Bath & Beauty", UrlSlug = "bath-and-beauty", CategoryId = 5 },
                new SubCategory { Id = 19, Name = "Gardening", UrlSlug = "gardening", CategoryId = 5 },
                new SubCategory { Id = 20, Name = "Pet Supplies", UrlSlug = "pet-supplies", CategoryId = 5 },
                new SubCategory { Id = 21, Name = "Handmade", UrlSlug = "handmade", CategoryId = 6 }
            );

            context.Products.AddOrUpdate(
                new Product { Id = Guid.NewGuid(), Name = "Product A", AvgRate = 1, Quantity = 50, OrderCounter = 5, Price = 100, UrlSlug = "product-a-1", SubCategoryId = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product B", AvgRate = 1, Quantity = 50, OrderCounter = 5, Price = 100, UrlSlug = "product-b-2", SubCategoryId = 1, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product C", AvgRate = 2, Quantity = 50, OrderCounter = 5, Price = 200, UrlSlug = "product-c-3", SubCategoryId = 2, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product D", AvgRate = 3, Quantity = 50, OrderCounter = 5, Price = 300, UrlSlug = "product-d-4", SubCategoryId = 3, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product E", AvgRate = 4, Quantity = 50, OrderCounter = 5, Price = 150, UrlSlug = "product-e-5", SubCategoryId = 4, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product F", AvgRate = 5, Quantity = 50, OrderCounter = 5, Price = 50, UrlSlug = "product-f-6", SubCategoryId = 5, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product G", AvgRate = 1, Quantity = 50, OrderCounter = 5, Price = 70, UrlSlug = "product-g-7", SubCategoryId = 6, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product H", AvgRate = 2, Quantity = 50, OrderCounter = 5, Price = 110, UrlSlug = "product-h-8", SubCategoryId = 1, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product I", AvgRate = 3, Quantity = 50, OrderCounter = 5, Price = 20, UrlSlug = "product-i-9", SubCategoryId = 2, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product J", AvgRate = 4, Quantity = 50, OrderCounter = 5, Price = 35, UrlSlug = "product-j-10", SubCategoryId = 4, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product K", AvgRate = 5, Quantity = 50, OrderCounter = 5, Price = 120, UrlSlug = "product-k-11", SubCategoryId = 6, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product L", AvgRate = 1, Quantity = 50, OrderCounter = 5, Price = 80, UrlSlug = "product-l-12", SubCategoryId = 1, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product M", AvgRate = 2, Quantity = 50, OrderCounter = 5, Price = 11, UrlSlug = "product-m-13", SubCategoryId = 7, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product N", AvgRate = 3, Quantity = 50, OrderCounter = 5, Price = 5, UrlSlug = "product-n-14", SubCategoryId = 8, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product O", AvgRate = 4, Quantity = 50, OrderCounter = 5, Price = 8, UrlSlug = "product-o-15", SubCategoryId = 3, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product P", AvgRate = 5, Quantity = 50, OrderCounter = 5, Price = 220, UrlSlug = "product-p-16", SubCategoryId = 1, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product Q", AvgRate = 1, Quantity = 50, OrderCounter = 5, Price = 350, UrlSlug = "product-q-17", SubCategoryId = 2, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product R", AvgRate = 3, Quantity = 50, OrderCounter = 5, Price = 330, UrlSlug = "product-r-18", SubCategoryId = 2, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product S", AvgRate = 2, Quantity = 50, OrderCounter = 5, Price = 270, UrlSlug = "product-s-19", SubCategoryId = 4, Status = 1  },
                new Product { Id = Guid.NewGuid(), Name = "Product T", AvgRate = 4, Quantity = 50, OrderCounter = 5, Price = 280, UrlSlug = "product-t-20", SubCategoryId = 5, Status = 1  }
            );
        }
    }
}
