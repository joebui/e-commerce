﻿using System;
using System.Collections.Generic;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Schema.Entity;

namespace eCommerce.Service
{
    public class AutoMappingConfiguration
    {
        public void Configure()
        {
            CategoryConfig();
        }

        private void CategoryConfig()
        {
#pragma warning disable 618
            Mapper.CreateMap<Category, CategoryDTO>().ReverseMap();
            Mapper.CreateMap<CategoryDTO, Category>().ReverseMap();
            Mapper.CreateMap<User, UserDTO>().ReverseMap();
            Mapper.CreateMap<Role, RoleDTO>().ReverseMap();
            Mapper.CreateMap<ProductPhoto, ProductPhotoDTO>().ReverseMap();
            Mapper.CreateMap<Product, ProductDTO>().ForMember(d => d.Status, m => m.MapFrom(s => s.Status))
                                                   .ForMember(d => d.CategoryName, m => m.MapFrom(s => s.SubCategory.Category.Name))
                                                   .ForMember(d => d.SubCategoryName, m => m.MapFrom(s => s.SubCategory.Name))
                                                   .ReverseMap();
            Mapper.CreateMap<SubCategory, SubCategoryDTO>().ReverseMap();
            Mapper.CreateMap<Order, OrderDTO>().ForMember(d => d.State, m => m.MapFrom(s => s.State)).ReverseMap();
            Mapper.CreateMap<OrderDetail, OrderDetailDTO>().ReverseMap().ForMember(x => x.Order, y => y.Ignore())
                .ForMember(x => x.OrderId, y => y.Ignore()).ForMember(x => x.Product, y => y.Ignore());
            Mapper.CreateMap<ProductReviewDTO, ProductReview>().ReverseMap();            
#pragma warning restore 618            
        }

        #region Singleton

        private static readonly Lazy<AutoMappingConfiguration> LazyInstance =
            new Lazy<AutoMappingConfiguration>(() => new AutoMappingConfiguration());

        public static AutoMappingConfiguration Instance
        {
            get { return LazyInstance.Value; }
        }

        #endregion
    }
}