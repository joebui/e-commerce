﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Controllers;
using Microsoft.AspNet.Identity;
using PagedList;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IOrderService _orderService;

        public UserController(IUserService userService, IRoleService roleService, IOrderService orderService)
        {
            _userService = userService;
            _roleService = roleService;
            _orderService = orderService;
        }

        // GET: Admin/User
        public ActionResult Index(int page = 1)
        {            
            ViewBag.Search = new SelectList(new List<string>(){ "All", "User", "Admin" });
            return View(_userService.GetAllUsers().OrderBy(u => u.UserName).ToPagedList(page, 10));
        }
        
        public ActionResult Remove(Guid id)
        {
            var count = _orderService.CountAvailableOrdersByUserId(id);
            if (count != 0) return Json(new { Url = Url.Action("ActionDenied") });

            _userService.RemoveUser(_userService.FindUserById(id));
            return User.Identity.GetUserId() == id.ToString()
                 ? Json(new { Url = Url.Action("Logout", "Auth", new { Area = "" }) })
                 : Json(new { Url = Url.Action("Index") });
        }

        // GET: Admin/User/UserDetails
        public ActionResult UserDetails(Guid id)
        {
            var user = _userService.FindUserById(id);
            ViewBag.RoleId = new SelectList(_roleService.GetAllRoles(), "Id", "Name", user.RoleId);
            return View(user);
        }

        // POST: Admin/User/UserDetails
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserDetails(UserDTO model)
        {
            _userService.UpdateUser(model);
            return RedirectToAction("Index");
        }

        // GET: Admin/User/ActionDenied
        public ActionResult ActionDenied()
        {
            return View();
        }
        
        [HttpPost]
        public PartialViewResult SearchByName(string result)
        {
            return PartialView("_SearchResult", _userService.GetUsersContainsName(result));
        }

        [HttpPost]
        public PartialViewResult SearchByRole(string search)
        {
            if (search == "All")
            {
                return PartialView("_SearchResult", _userService.GetAllUsers().OrderBy(u => u.UserName));
            }
            else
            {
                return PartialView("_SearchResult", _userService.GetUsersWithRole(search));
            }            
        }
    }
}