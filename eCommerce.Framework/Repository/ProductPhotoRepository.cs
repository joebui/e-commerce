﻿using eCommerce.Framework.Infrastructure;
using eCommerce.Schema.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Framework.Repository
{
    public interface IProductPhotoRepository : IRepository<ProductPhoto>
    {

    }
    public class ProductPhotoRepository : RepositoryBase<ProductPhoto>, IProductPhotoRepository
    {
        public ProductPhotoRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
