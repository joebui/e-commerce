﻿using eCommerce.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.IService
{
    public interface ICategoryService
    {
        IEnumerable<CategoryDTO> GetAllCategory();

        CategoryDTO GetCategoryById(int id);
        CategoryDTO GetCategoryByName(string name);

        bool IsThisCategoryAvailable(string name);

        void Create(CategoryDTO categoryDto);
        void Update(CategoryDTO categoryDTO);
        void Delete(CategoryDTO categoryDTO);
        void Save();
    }
}
