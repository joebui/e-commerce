﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ManageUserAndOrder
    {
        public RegisterModel RegisterModel { get; set; }
        public List<OrderManageViewModel> OrderManageViewModel { get; set; }
    }
}