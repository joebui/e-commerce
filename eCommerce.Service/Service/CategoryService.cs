﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(ICategoryRepository categoryRepository, IUnitOfWork unitOfWork)
        {
            _categoryRepository = categoryRepository;
            _unitOfWork = unitOfWork;
        }

        public CategoryDTO GetCategoryById(int id)
        {
            return Mapper.Map<Category, CategoryDTO>(_categoryRepository.GetById(id));
        }

        public CategoryDTO GetCategoryByName(string name)
        {
            return Mapper.Map<Category, CategoryDTO>(_categoryRepository.Get(c => c.Name.Equals(name)));
        }

        public bool IsThisCategoryAvailable(string name)
        {            
            return _categoryRepository.GetMany(c => c.Name.Equals(name)).Any();
        }

        public void Create(CategoryDTO categoryDto)
        {
            _categoryRepository.Add(Mapper.Map<CategoryDTO, Category>(categoryDto));
            _unitOfWork.Commit();
        }

        public void Delete(CategoryDTO categoryDTO)
        {
            categoryDTO.IsDeleted = true;

            _categoryRepository.AddOrUpdate(Mapper.Map<CategoryDTO, Category>(categoryDTO));
            _unitOfWork.Commit();
        }

        public IEnumerable<CategoryDTO> GetAllCategory()
        {
            var categories = _categoryRepository.GetMany(c => !c.IsDeleted).ToList();
            return Mapper.Map<List<Category>, List<CategoryDTO>>(categories);
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(CategoryDTO categoryDTO)
        {
            _categoryRepository.Update(Mapper.Map<CategoryDTO, Category>(categoryDTO));
            _unitOfWork.Commit();
        }
    }
}
