﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class SubCategoryModel
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }

        public virtual CategoryModel Category { get; set; }
    }
}