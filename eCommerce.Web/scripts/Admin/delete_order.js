﻿$(document).ready(function () {
    var url = $("#url_delete_order").val();
    $('body').on('click', '.delete_order', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modalDeleteOrder').data('id', id).modal('show');        
    });

    $('#btnConfirmDelete').click(function () {
        var id = $('#modalDeleteOrder').data('id');
        $.ajax({
            url: url,
            data: { 'id': id },
            type: 'GET',
            success: function () {
                $('a[data-id="' + id + '"]').closest('tr').hide('fast');
            },
            error: function () {
                alert(url);
            }
        });
        $('#modalDeleteOrder').modal('hide');
    });
})