﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.Web.Controllers
{
    public class BaseController : Controller
    {
        public ActionResult NotFound()
        {
            return HttpNotFound();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var logFile = Server.MapPath("~/Log/ErrorLog.txt");
            WriteLog(logFile, filterContext.Exception);

            // Output a nice error page            
            filterContext.ExceptionHandled = true;
            filterContext.Result = this.RedirectToAction("Error", "Home", new {Area = ""});            
        }

        private static void WriteLog(string logFile, Exception ex)
        {
            // Open the log file for append and write the log
            var sw = new StreamWriter(logFile, true);
            sw.WriteLine($"********** {DateTime.Now} **********");
            if (ex.InnerException != null)
            {
                sw.Write("Inner Exception Type: ");
                sw.WriteLine(ex.InnerException.GetType().ToString());
                sw.Write("Inner Exception: ");
                sw.WriteLine(ex.InnerException.Message);
                sw.Write("Inner Source: ");
                sw.WriteLine(ex.InnerException.Source);
                if (ex.InnerException.StackTrace != null)
                {
                    sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(ex.InnerException.StackTrace);
                }
            }
            sw.Write("Exception Type: ");
            sw.WriteLine(ex.GetType().ToString());
            sw.WriteLine("Exception: " + ex.Message);
            sw.WriteLine("Source: " + ex.TargetSite);
            sw.WriteLine("Stack Trace: ");
            if (ex.StackTrace != null)
            {
                sw.WriteLine(ex.StackTrace);
                sw.WriteLine();
            }
            sw.Close();
        }
    }
}