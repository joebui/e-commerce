﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class RoleDTO : BaseDTO<int>
    {
        public string Name { get; set; }

        public virtual IList<UserDTO> Users { get; set; }
    }
}
