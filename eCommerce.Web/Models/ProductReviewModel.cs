﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eCommerce.Web.Models
{
    public class ProductReviewModel
    {        
        public string ReviewContent { get; set; }
        public int Rate { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}