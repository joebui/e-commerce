﻿using System;
using Autofac;
using eCommerce.Framework.Repository;

namespace eCommerce.Service
{
    public class IocRegister
    {
        public void Configure(ContainerBuilder container)
        {
            container.RegisterAssemblyTypes(typeof(CategoryRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

            AutoMappingConfiguration.Instance.Configure();
        }

        #region Singletons

        private static readonly Lazy<IocRegister> LazyInstance = new Lazy<IocRegister>(() => new IocRegister());

        public static IocRegister Instance
        {
            get { return LazyInstance.Value; }
        }

        #endregion
    }
}
