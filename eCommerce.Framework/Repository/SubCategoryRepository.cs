﻿using eCommerce.Framework.Infrastructure;
using eCommerce.Schema.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Framework.Repository
{
    public interface ISubCategoryRepository : IRepository<SubCategory>
    {

    }
    public class SubCategoryRepository : RepositoryBase<SubCategory>, ISubCategoryRepository
    {
        public SubCategoryRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
