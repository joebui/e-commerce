﻿using System;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Web.Areas.Admin.Models;
using eCommerce.Web.Models;
using CategoryModel = eCommerce.Web.Models.CategoryModel;

namespace eCommerce.Web.Library
{
    public class AutoMappingConfiguration
    {
        public void Configure()
        {
            CategoryConfig();
        }

        private void CategoryConfig()
        {
#pragma warning disable 618
            Mapper.CreateMap<CategoryDTO, CategoryModel>().ReverseMap();
            Mapper.CreateMap<RegisterModel, UserDTO>().ReverseMap();
            Mapper.CreateMap<Models.ProductFormViewModel, ProductDTO>().ReverseMap();
            Mapper.CreateMap<Areas.Admin.Models.ProductFormViewModel, ProductDTO>();
            Mapper.CreateMap<ProductDTO, Areas.Admin.Models.ProductFormViewModel>().ForMember(d => d.CategoryId, m => m.MapFrom(s => s.SubCategory.Category.Id));
            Mapper.CreateMap<ProductDTO, ProductModel>().ReverseMap();
            Mapper.CreateMap<Areas.Admin.Models.CategoryModel, CategoryDTO>().ReverseMap();
            Mapper.CreateMap<SubCategoryDTO, SubcategoryModel>().ReverseMap();
            Mapper.CreateMap<SubCategoryModel, SubCategoryDTO>().ReverseMap();
            Mapper.CreateMap<OrderDTO, OrderManageViewModel>().ReverseMap();
            Mapper.CreateMap<OrderDetailDTO, OrderDetailViewModel>().ReverseMap();
            Mapper.CreateMap<UserDTO, UserViewModel>().ReverseMap();
            Mapper.CreateMap<ReviewFormViewModel, ProductReviewDTO>().ReverseMap();
            Mapper.CreateMap<UserCheckoutModel, UserDTO>().ReverseMap();
            Mapper.CreateMap<ProductDTO, ProductDetailModel>().ReverseMap();
            Mapper.CreateMap<ProductPhotoDTO, ProductPhotoModel>().ReverseMap();
            Mapper.CreateMap<ProductReviewDTO, ProductReviewModel>().ForMember(d => d.UserName, m => m.MapFrom(s => s.User.UserName)).ReverseMap();
            Mapper.CreateMap<UserDTO, UserBillingModel>().ReverseMap();
#pragma warning restore 618            
        }

        #region Singleton

        private static readonly Lazy<AutoMappingConfiguration> LazyInstance =
            new Lazy<AutoMappingConfiguration>(() => new AutoMappingConfiguration());

        public static AutoMappingConfiguration Instance
        {
            get { return LazyInstance.Value; }
        }

        #endregion
    }
}