﻿$(document).ready(function () {
    var url = $("#url").val();
    $(".delete-cate").on("click", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        $("#modalDeleteCategory").data("id", id).modal("show");
    });

    $("#btnConfirmDelete").click(function () {
        var id = $("#modalDeleteCategory").data("id");
        $.ajax({
            type: "POST",
            url: url,
            data: { 'id': id },           
            dataType: "json",
            success: function (response) {
                window.location = response.Url;                
            },
            error: function (response) {
                alert("Failed");
            }
        });
        $("#modalDeleteCategory").modal("hide");
    });
})