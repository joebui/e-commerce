﻿namespace eCommerce.Web.Models
{
    public class UserCheckoutModel
    {
        public string UserName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}