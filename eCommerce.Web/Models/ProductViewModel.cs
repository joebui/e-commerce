﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ProductViewModel
    {
        public IPagedList<ProductModel> Products { get; set; }
        public IList<CategoryModel> Categories { get; set; }
        public IList<ProductModel> Recommended { get; set; }
    }
}