﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace eCommerce.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js",
            "~/Scripts/jquery.unobtrusive-ajax.min.js",
            "~/Scripts/cartcalculation.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                         "~/Scripts/bootstrap.js",
                         "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/web").Include(
                    "~/Scripts/contact.js",
                    "~/Scripts/gmaps.js",
                    "~/Scripts/html5shiv.js",
                    "~/Scripts/jquery.prettyPhoto.js",
                    "~/Scripts/jquery.scrollUp.min.js",
                    "~/Scripts/main.js",
                    "~/Scripts/price-range.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/animate.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/main.css",
                      "~/Content/prettyPhoto.css",
                      "~/Content/price-range.css",
                      "~/Content/responsive.css"));

            //Admin pages
            bundles.Add(new ScriptBundle("~/bundles/AdminScript").Include(
                "~/Scripts/Admin/custom.js",
                "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/AdminStyle").Include(
                "~/Content/bootstrap.css",
                "~/Content/Admin/styles.css"));

            bundles.Add(new ScriptBundle("~/bundles/AdminLoadSubCategory").Include(
               "~/Scripts/Admin/loadSubCategory.js"));

            //Admin product index
            bundles.Add(new ScriptBundle("~/bundles/AdminProductIndex").Include(
                "~/Scripts/Admin/product_index.js"));

            //Product create page
            bundles.Add(new ScriptBundle("~/bundles/loadSubCategory").Include(
               "~/Scripts/loadSubCategory.js"));

            //Product detail page
            bundles.Add(new ScriptBundle("~/bundles/product_detail").Include(
               "~/Scripts/productDetail.js",
               "~/Scripts/rating.js"));
        }
    }
}