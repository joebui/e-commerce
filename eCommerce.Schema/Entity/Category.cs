﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class Category : BaseEntity<int>
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }

        public virtual IList<SubCategory> SubCategories { get; set; }
    }
}
