﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Domain.DTO;

namespace eCommerce.Domain.IService
{
    public interface IUserService
    {
        UserDTO CheckRegisterUsernameEmail(string name, string email);
        UserDTO CheckLoginUsernamEmail(string nameEmail);
        UserDTO FindUserByName(string name);
        UserDTO FindUserById(Guid id);

        IEnumerable<UserDTO> GetAllUsers();
        IEnumerable<UserDTO> GetUsersContainsName(string name);
        IEnumerable<UserDTO> GetUsersWithRole(string role);

        void AddNewUser(UserDTO userDto);
        void UpdateUser(UserDTO userDto);
        void RemoveUser(UserDTO userDto);
    }
}
