﻿using eCommerce.Domain.DTO;
using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ProductDetailModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int AvgRate { get; set; }
        public int Quantity { get; set; }
        public string RepresentImg { get; set; }
        public string Description { get; set; }
        public ProductState Status { get; set; }
        public IList<ProductPhotoModel> ProductPhotos { get; set; }
        public IList<ProductReviewModel> ProductReviews { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
    }
}