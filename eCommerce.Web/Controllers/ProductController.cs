﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using PagedList;

namespace eCommerce.Web.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IProductReviewService _productReviewService;
        private readonly ICategoryService _categoryService;
        private readonly ISubCategoryService _subCategoryService;

        public ProductController(IProductService productService, ICategoryService categoryService, ISubCategoryService subCategoryService, IProductReviewService productReviewService)
        {
            _productService = productService;
            _productReviewService = productReviewService;
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
        }

        // GET: Product
        public ActionResult Index(string categoryFilter, string sortOrder,int page=1, int pagesize = 12)
        {
            categoryFilter = (string.IsNullOrEmpty(categoryFilter)) ? "" : categoryFilter;
            ViewBag.CategoryFilter = categoryFilter;
            ViewBag.CategoryName = _subCategoryService.GetSubCategoryBySlug(categoryFilter).Name;
            ViewBag.SortBy = (string.IsNullOrEmpty(sortOrder)) ? "price_desc" : sortOrder;
            ViewBag.Pagesize = pagesize;


            var productsDto = _productService.GetProductsBySubCategorySlug(categoryFilter);

            switch (sortOrder)
            {
                case "arrival_asc":
                    productsDto = productsDto.OrderBy(p => p.CreatedDate);
                    break;
                case "arrival_desc":
                    productsDto = productsDto.OrderByDescending(p => p.CreatedDate);
                    break;
                case "name_asc":
                    productsDto = productsDto.OrderBy(p => p.Name);
                    break;
                case "name_desc":
                    productsDto = productsDto.OrderByDescending(p => p.Name);
                    break;
                case "price_asc":
                    productsDto = productsDto.OrderBy(p => p.Price);
                    break;
                default:
                    productsDto = productsDto.OrderByDescending(p => p.Price);
                    break;
            }
            var productVm = new ProductViewModel();
            productVm.Products = Mapper.Map<List<ProductDTO>, List<ProductModel>>(productsDto.ToList()).ToPagedList(page,pagesize);
            productVm.Categories = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(_categoryService.GetAllCategory().ToList());

            return View(productVm);
        }       

        public ActionResult Detail(string slug)
        {
            if (slug == null) return RedirectToAction("Index");

            var productDto = _productService.GetBySlug(slug);
            if (productDto == null) return Redirect("~/ErrorPages/404.html");

            var viewModel = new ProductDetailViewModel();
            viewModel.ProductDetail = Mapper.Map<ProductDTO, ProductDetailModel>(productDto);
            viewModel.Categories = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(_categoryService.GetAllCategory().ToList());
            return View(viewModel);
        }

        public ActionResult DetailById(Guid id)
        {
            var productDto = _productService.GetById(id);
            if (productDto == null)
            {
                return HttpNotFound();
            }
            var viewModel = new ProductDetailViewModel();
            viewModel.ProductDetail = Mapper.Map<ProductDTO, ProductDetailModel>(productDto);
            viewModel.Categories = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(_categoryService.GetAllCategory().ToList());

            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddComment(ReviewFormViewModel viewModel)
        {
            var reviewDto = Mapper.Map<ReviewFormViewModel, ProductReviewDTO>(viewModel);
            reviewDto.UserId = Guid.Parse(User.Identity.GetUserId());
            _productReviewService.AddReview(reviewDto);
            _productReviewService.Save();
            _productService.UpdateRating(reviewDto.ProductId);
            _productService.Save();
            var reviewModel = Mapper.Map<ProductReviewDTO, ProductReviewModel>(reviewDto);
            reviewModel.CreatedDate = DateTime.Now;
            reviewModel.UserName = User.Identity.Name;
            return PartialView("_Comment", reviewModel);
        }

        [Authorize]
        public ActionResult Create()
        {
            var viewModel = new ProductFormViewModel();
            viewModel.Categories = GetCategory();
            viewModel.Price = 1.00M;
            viewModel.SubCategories = new[] { new SelectListItem { Value = "", Text = "" } };
            return View(viewModel);            
        }
        
        [HttpPost]
        public ActionResult Create(ProductFormViewModel viewModel)
        {
            var productDTO = Mapper.Map<ProductFormViewModel, ProductDTO>(viewModel);
            productDTO.CreatedBy = Guid.Parse(User.Identity.GetUserId());
            _productService.Create(productDTO);
            _productService.Save();
            TempData["Message"] = viewModel.Name + " has been uploaded and is awaiting for administrator's approval.";
            return RedirectToAction("Index", "Home");
        }

        private IEnumerable<SelectListItem> GetCategory()
        {
            var categories = _categoryService.GetAllCategory();
            var list = new List<SelectListItem>();
            foreach (var c in categories)
            {
                list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
            }

            return list;
        }

        public ActionResult GetSubCategory(int id)
        {
            var subCategories = _subCategoryService.GetSubCategoriesByCategory(id);
            var list = new List<SelectListItem>();
            foreach (var c in subCategories)
            {
                list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}