# Gifts4Charity website

## Getting started

-   Run this command in Package Manager Console before running the project for the first time.
```
Update-Database -TargetMigration FirstInit
```
-   To login as an admin, use this default account - Username: admin, Password: admin1234
-   Make sure to install NUnit3 Test Adapter before running unit tests by going to Tools > Extensions and Updates > 
Search "NUnit3 Test Adapter" and install
