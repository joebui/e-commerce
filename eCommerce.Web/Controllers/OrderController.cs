﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Ultility;
using eCommerce.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using eCommerce.Ultility.EmailHandler;
using Microsoft.AspNet.Identity;

namespace eCommerce.Web.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public OrderController(IProductService productService, IOrderService orderService, IUserService userService)
        {
            _productService = productService;
            _orderService = orderService;
            _userService = userService;
        }

        public ActionResult Index()
        {
            var ordersDto = _orderService.GetOrderByUserId(_userService.FindUserByName(User.Identity.Name).Id);
            var ordersVm = Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(ordersDto.ToList());

            return View(ordersVm);
        }

        public ActionResult OrderDetail(Guid orderId)
        {
            var orderDetailsDto = _orderService.GetOrderDetail(orderId);
            var orderDetailsVm = Mapper.Map<List<OrderDetailDTO>, List<OrderDetailViewModel>>(orderDetailsDto.ToList());

            return View(orderDetailsVm);
        }

        public ActionResult AddToCart(Guid productId, string name, decimal price, string slug, string RepresentImg, int Stock)
        {
            UpdateCart(productId, null, name, price, slug, RepresentImg, Stock);
            var cart = (List<CartItem>)Session["Cart"];
            var data = cart.FirstOrDefault(x => x.ItemId == productId);
            return PartialView("_AddedToCart", data);
        }        

        public ActionResult CartPage()
        {
            ViewBag.IsCheckout = false;
            ValidateCart();
            return View(Session["Cart"]);
        }

        [Authorize]
        public ActionResult CheckoutPage()
        {
            ViewBag.IsCheckout = true;
            var userDto = _userService.FindUserByName(User.Identity.Name);
            var user = Mapper.Map<UserDTO, UserCheckoutModel>(userDto);

            ValidateCart();

            return View("Checkout", user);
        }

        public async Task<ActionResult> ProceedOrder()
        {
            var cart = (List<CartItem>)Session["Cart"];
            var cartForEmail = new List<string>();

            if (cart == null || cart.Count == 0)
            {
                TempData["Message"] = Constant.MESSAGE_ORDER_EMPTY;
                return RedirectToAction("Index", "Home");
            }
            var orderDetails = new List<OrderDetailDTO>();
            foreach (var x in cart)
            {
                orderDetails.Add(new OrderDetailDTO { Quantity = x.Quantity, ProductId = x.ItemId });
                AddItemToCartForEmail(cartForEmail, x);
            }

            var user = _userService.FindUserByName(User.Identity.Name);
            var orderId = _orderService.Create(user.Id, (int)Session["CartTotal"], orderDetails);

            cartForEmail.Add("Total Price: " + cart.Sum(c => c.Price * c.Quantity));
            cartForEmail.Add("ORDER ID: " + orderId);            

            //Clear session
            cart.Clear();
            Session["CartTotal"] = 0;

            // Send email to user.
            await CheckOutConfirmation.Send(user.Email, cartForEmail);
            return RedirectToAction("Confirmation");

        }        

        [HttpPost]
        public ActionResult AddToCartInDetailPage(Guid productId, int quantity)
        {
            var product = _productService.GetById(productId);
            UpdateCart(productId, quantity, product.Name, product.Price, product.UrlSlug, product.RepresentImg, product.Quantity);
            TempData["Message"] = quantity + " " + product.Name + " have been added to cart.";
            return RedirectToAction("Detail", "Product", new { slug = product.UrlSlug });
        }

        public ActionResult Confirmation()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BillingInfo(string address, string phoneNumber)
        {
            var user = _userService.FindUserById(Guid.Parse(User.Identity.GetUserId()));
            user.Address = address;
            user.PhoneNumber = phoneNumber;
            _userService.UpdateUser(user);

            return RedirectToAction("CheckoutPage");
        }

        #region OrderFunctions
        private void UpdateCart(Guid id, int? quantity, string name, decimal price, string slug, string representImg, int Stock)
        {
            if (quantity == null)
                quantity = 1;
            List<CartItem> cart;
            if (Session["Cart"] == null)
            {
                cart = new List<CartItem>();
                cart.Add(new CartItem { ItemId = id, Quantity = (int)quantity, Name = name, Price = price, Slug = slug, RepresentImg = representImg });
                Session["Cart"] = cart;
                Session["CartTotal"] = quantity;
            }
            else
            {
                var itemExisted = false;
                cart = (List<CartItem>)Session["Cart"];
                foreach (var item in cart)
                {
                    if (item.ItemId == id)
                    {
                        itemExisted = true;
                        item.Quantity += (int)quantity;
      
                        break;
                    }
                }

                if (!itemExisted)
                {
                    cart.Add(new CartItem { ItemId = id, Quantity = (int)quantity, Price = price, Name = name, Slug = slug, RepresentImg = representImg });
                }

                Session["Cart"] = cart;
                Session["CartTotal"] = cart.Sum(x => x.Quantity);

            }

        }

        public void UpdateCartItem(string ItemId, string actionName)
        {
            var cart = (List<CartItem>)Session["Cart"];
            var item = cart.FirstOrDefault(x => x.ItemId == new Guid(ItemId));
            switch (actionName)
            {
                case "increase":
                    item.Quantity++;
                    break;
                case "decrease":
                    if (item.Quantity > 1)
                        item.Quantity--;
                    break;
                case "delete":
                    cart.Remove(item);
                    break;
            }

            Session["CartTotal"] = cart.Sum(x => x.Quantity);
        }                

        public void ValidateCart()
        {
            var exceedStock = false;
            var sb = new StringBuilder();
            sb.Append("The following item(s) have exceeded our stock. Their quantity has been adjusted: <br/>");

            // Validate products in cart actually exist in database and quantity does not exceed stock.
            var cart = (List<CartItem>)Session["Cart"];
            if (cart == null)
                return;
            for (int x = 0; x < cart.Count; x++)
            {
                var y = _productService.GetById(cart[x].ItemId);
                if (y == null)
                {
                    cart.RemoveAt(x);
                }
                if (y.Quantity < cart[x].Quantity)
                {
                    exceedStock = true;
                    sb.Append("<li>" + cart[x].Name + " (Original: " + cart[x].Quantity + ", Adjusted: " + y.Quantity + ")</li>");
                    Session["CartTotal"] = (int)Session["CartTotal"] + y.Quantity - cart[x].Quantity;
                    cart[x].Quantity = y.Quantity;
                    
                }
                if (exceedStock)
                {
                    TempData["Message"] = sb.ToString();
                }
            }
        }

        public void CancelOrder(Guid orderId)
        {
            _orderService.CancelOrder(orderId);
        }

        public void AddItemToCartForEmail(List<string> cartString, CartItem item)
        {
            var builder = new StringBuilder();
            builder.Append("Name: ");
            builder.Append(item.Name);
            builder.Append(" _ ");

            builder.Append("Price: ");
            builder.Append(item.Price);
            builder.Append(" _ ");

            builder.Append("Quantity: ");
            builder.Append(item.Quantity);
            builder.Append(" _ ");

            builder.Append("Total: ");
            builder.Append(item.Price * item.Quantity);            

            cartString.Add(builder.ToString());            
        }
        #endregion
    }
}