﻿using eCommerce.Domain.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using eCommerce.Domain.DTO;
using eCommerce.Framework.Repository;
using eCommerce.Framework.Infrastructure;
using AutoMapper;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class SubCategoryService : ISubCategoryService
    {
        private readonly ISubCategoryRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public SubCategoryService(ISubCategoryRepository subCategoryRepository, IUnitOfWork unitOfWork)
        {
            _repository = subCategoryRepository;
            _unitOfWork = unitOfWork;
        }

        public SubCategoryDTO GetSubCategoryById(int id)
        {
            var sub = _repository.GetById(id);
            return Mapper.Map<SubCategory, SubCategoryDTO>(sub);
        }

        public SubCategoryDTO GetSubCategoryBySlug(string slug)
        {
            return Mapper.Map<SubCategory, SubCategoryDTO>(_repository.Get(c => c.UrlSlug.Equals(slug)));
        }

        public bool IsThisSubcategoryAvailable(string name)
        {
            return _repository.GetMany(s => s.Name.Equals(name)).Any();
        }

        public bool AreSubcategoriesOfCategoryDeleted(int categoryId)
        {
            var sub = _repository.GetMany(s => s.CategoryId == categoryId && s.IsDeleted).Any();
            return sub;
        }

        public void Create(SubCategoryDTO subCategoryDto)
        {
            _repository.Add(Mapper.Map<SubCategoryDTO, SubCategory>(subCategoryDto));
            _unitOfWork.Commit();
        }

        public void Delete(SubCategoryDTO subCategoryDTO)
        {
            subCategoryDTO.IsDeleted = true;

            _repository.AddOrUpdate(Mapper.Map<SubCategoryDTO, SubCategory>(subCategoryDTO));
            _unitOfWork.Commit();
        }

        public IEnumerable<SubCategoryDTO> GetAll()
        {
            var subCategories = _repository.GetMany(sc => sc.IsDeleted == false).ToList();            
            return Mapper.Map<List<SubCategory>, List<SubCategoryDTO>>(subCategories);
        }

        public IEnumerable<SubCategoryDTO> GetSubCategoriesByCategory(int categoryId)
        {
            var subCategories = _repository.GetMany(s => s.CategoryId == categoryId && !s.IsDeleted).ToList();            
            return Mapper.Map<List<SubCategory>, List<SubCategoryDTO>>(subCategories);
        }        

        public void Update(SubCategoryDTO subCategoryDTO)
        {
            _repository.Update(Mapper.Map<SubCategoryDTO, SubCategory>(subCategoryDTO));
            _unitOfWork.Commit();
        }
    }
}
