﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Service.Service;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;

namespace eCommerce.UnitTest.ServiceTest
{
    class CategoryServiceTests
    {
        private Mock<ICategoryRepository> _repo;
        private ICategoryService _service;
        private Mock<IUnitOfWork> _unit;
        private readonly List<Category> _categories = new List<Category>();
        private readonly List<Category> _emptyCategories = new List<Category>();

        [SetUp]
        public void Setup()
        {
            _repo = new Mock<ICategoryRepository>();
            _unit = new Mock<IUnitOfWork>();
            _service = new CategoryService(_repo.Object, _unit.Object);
            AutoMappingRegistration.Register();

            _categories.Add(new Category() { Id = 1, Name = "Handmade" });
            _categories.Add(new Category() { Id = 2, Name = "Furniture" });
            _categories.Add(new Category() { Id = 3, Name = "Clothing" });
            _categories.Add(new Category() { Id = 4, Name = "Peripherals" });
        }
        
        [TestCase(1)]
        public void Get_Category_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns(_categories.First());

            var result = _service.GetCategoryById(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.Id);
        }

        [TestCase(5)]
        public void Unable_To_Get_Category_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns((Category) null);

            var result = _service.GetCategoryById(id);

            Assert.IsNull(result);            
        }

        [TestCase("Handmade")]
        public void Get_Category_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<Category, bool>>>())).Returns(_categories.First());

            var result = _service.GetCategoryByName(name);

            Assert.IsNotNull(result);
            Assert.AreEqual(name, result.Name);
        }

        [TestCase("Furniture")]
        public void Unable_To_Get_Category_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<Category, bool>>>())).Returns((Category) null);

            var result = _service.GetCategoryByName(name);

            Assert.IsNull(result);
        }
        
        [TestCase("Handmade")]
        public void Category_Is_Available(string name)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<Category, bool>>>())).Returns(_categories);

            var result = _service.IsThisCategoryAvailable(name);

            Assert.IsTrue(result);            
        }

        [TestCase("Phone")]
        public void Category_Is_Unavailable(string name)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<Category, bool>>>())).Returns(_emptyCategories);

            var result = _service.IsThisCategoryAvailable(name);

            Assert.IsFalse(result);
        }

        [Test]
        public void Create_Category()
        {
            var category = Mapper.Map<Category, CategoryDTO>(_categories.First());

            _service.Create(category);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Delete_Category()
        {
            var category = Mapper.Map<Category, CategoryDTO>(_categories.First());

            _service.Delete(category);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Update_Category()
        {
            var category = Mapper.Map<Category, CategoryDTO>(_categories.First());

            _service.Update(category);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Get_All_Category()
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<Category, bool>>>())).Returns(_categories);

            var result = _service.GetAllCategory();

            Assert.IsNotNull(result);
            Assert.AreEqual(_categories.Count, result.Count());
        }
    }
}
