﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class CartItem
    {
        public Guid ItemId { get; set; }
        public int Quantity { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Slug { get; set; }

        public string RepresentImg { get; set; }


    }
}