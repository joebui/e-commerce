﻿$(document).ready(function () {
    var url = $('#url_approveOrder').val();
    $('#btn_approve').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modalApproveOrder').data('id', id).modal('show');
    })

    $('#btnConfirmApprove').click(function () {
        var id = $('#modalApproveOrder').data('id');
        $.ajax({
            url: url,
            data: { 'orderId': id },
            type: 'GET',
            success: function () {
                $('#order_state').html('<label>State: </label><span class="label label-primary">Processed</span>');
                $('#btn_approve').hide();
                $('#btn_cancel').hide();                
            },
            error: function () {
                alert("failed");
            }
        })

        $('#modalApproveOrder').modal('hide');
    })
})