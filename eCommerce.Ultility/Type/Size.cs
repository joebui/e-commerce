﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Ultility.Type
{
    public enum Size
    {
        XS,
        S,
        M,
        L,
        XL,
        XXL
    }
}
