﻿$(document).ready(function () {
    var url = $('#cancel_order_url').val();
    $('.cart_quantity_delete').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modalCancelOrder').data('id', id).modal('show');
    })
    $('#btnConfirm').click(function () {
        var id = $('#modalCancelOrder').data('id');
        $.ajax({
            url: url,
            data: { 'orderId': id },
            type: 'GET',
            success: function () {
                $('a[data-id="' + id + '"]').closest('td').hide('slow');
                $('a[data-id="' + id + '"]').closest('td').prev().html('<h5 class="state_result">Canceled</h5>');
            },
            error: function () {
                alert("failed");
            }
        })
        $('#modalCancelOrder').modal('hide');
    })
})