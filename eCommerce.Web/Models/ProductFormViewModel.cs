﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.Web.Models
{
    public class ProductFormViewModel
    {
        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Price")]
        [Range(typeof(decimal), "1", "9999")]
        public decimal Price { get; set; }
        
        [Required]
        [DisplayName("Quantity")]
        [Range(1, int.MaxValue)]
        public int Quantity { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Category")]
        public string SubCategoryId { get; set; }
        public Guid CreatedBy { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        public IEnumerable<SelectListItem> SubCategories { get; set; }
        public List<HttpPostedFileBase> Images { get; set; }
    }
}