﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ProductDetailViewModel
    {
        public IList<CategoryModel> Categories { get; set; }
        public ProductDetailModel ProductDetail { get; set; }
        [Required(ErrorMessage = "Please enter your comment")]
        public string ReviewContent { get; set; }

    }
}