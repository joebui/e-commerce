﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class RegisterModel
    {        
        [Key]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("User Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public virtual string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The Email field is invalid")]
        public virtual string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public virtual string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Retype Password mismatched")]
        [DisplayName("Confirm Password")]
        public virtual string RetypePassword { get; set; }

        [Required]        
        public virtual string Address { get; set; }

        [Required]
        [DisplayName("Phone")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "The phone number field is invalid")]
        public virtual string PhoneNumber { get; set; }

        public int RoleId { get; set; }
    }
}