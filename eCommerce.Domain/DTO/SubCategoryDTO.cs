﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class SubCategoryDTO : BaseDTO<int>
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public int CategoryId { get; set; }

        public virtual IList<ProductDTO> Products { get; set; }
        public virtual CategoryDTO Category { get; set; }
    }
}
