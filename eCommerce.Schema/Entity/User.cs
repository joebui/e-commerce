﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class User : BaseEntity<Guid>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual IList<ProductReview> ProductReviews { get; set; }
        public virtual IList<Order> Orders{ get; set; }
    }
}
