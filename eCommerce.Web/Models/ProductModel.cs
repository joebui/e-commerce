﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ProductModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string RepresentImg { get; set; }
        public string UrlSlug { get; set; }
        public string Description { get; set; }
        public int AvgRate { get; set; }
        public ProductState Status { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public int Quantity { get; set; }
    }
}