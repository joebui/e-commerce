﻿using System;
using System.Collections.Generic;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Domain.DTO;
using AutoMapper;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class ProductReviewService : IProductReviewService
    {
        private readonly IProductReviewRepository _reviewRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductReviewService(IProductReviewRepository reviewRepository, IUnitOfWork unitOfWork)
        {
            _reviewRepository = reviewRepository;            
            _unitOfWork = unitOfWork;
        }

        public void AddReview(ProductReviewDTO productReviewDTO)
        {
            var productReview = Mapper.Map<ProductReviewDTO, ProductReview>(productReviewDTO);
            productReview.Id = Guid.NewGuid();
            productReview.CreatedDate = DateTime.Now;
            _reviewRepository.Add(productReview);            
        }

        public IEnumerable<ProductReviewDTO> LoadReviews(Guid productId)
        {
            var productReviews = _reviewRepository.GetMany(r => r.ProductId == productId);
            return Mapper.Map<IEnumerable<ProductReview>, IEnumerable<ProductReviewDTO>>(productReviews); ;            
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }        
    }
}
