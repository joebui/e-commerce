﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Ultility
{
    public class Constant
    {
        public const string MESSAGE = "This is a message.";
        public const string MESSAGE_ORDER_SUCCESS = "Your order is now being processed. " +
                                                    "An email is sent to you to confirm your checkout orders.";
        public const string MESSAGE_ORDER_EMPTY = "Your current cart is empty. Go order something!";
        public const string EncryptionKey = "MAKV2SPBNI99212";
    }
}
