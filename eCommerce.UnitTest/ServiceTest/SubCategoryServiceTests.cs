﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Service.Service;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;

namespace eCommerce.UnitTest.ServiceTest
{
    class SubCategoryServiceTests
    {
        private Mock<ISubCategoryRepository> _repo;
        private Mock<IUnitOfWork> _unit;
        private ISubCategoryService _service;
        private readonly List<SubCategory> _subCategories = new List<SubCategory>();
        private readonly List<SubCategory> _emptyCategories = new List<SubCategory>();

        [SetUp]
        public void Setup()
        {
            _repo = new Mock<ISubCategoryRepository>();
            _unit = new Mock<IUnitOfWork>();
            _service = new SubCategoryService(_repo.Object, _unit.Object);
            AutoMappingRegistration.Register();

            _subCategories.Add(new SubCategory() { Id = 1, Name = "Sub 1", CategoryId = 4, UrlSlug = "slug1-1"});
            _subCategories.Add(new SubCategory() { Id = 2, Name = "Sub 2", CategoryId = 4, UrlSlug = "slug2-2" });
            _subCategories.Add(new SubCategory() { Id = 3, Name = "Sub 3", CategoryId = 4, UrlSlug = "slug3-3" });
            _subCategories.Add(new SubCategory() { Id = 4, Name = "Sub 4", CategoryId = 4, UrlSlug = "slug4-4" });
        }
        
        [TestCase(1)]
        public void Get_Subcategory_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns(_subCategories.First());

            var result = _service.GetSubCategoryById(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.Id);
        }

        [TestCase(2)]
        public void Unable_To_Get_Subcategory_By_Id(int id)
        {
            _repo.Setup(r => r.GetById(id)).Returns((SubCategory) null);

            var result = _service.GetSubCategoryById(id);

            Assert.IsNull(result);            
        }

        [TestCase("slug1-1")]
        public void Get_Subcategory_By_Slug(string slug)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_subCategories.First());

            var result = _service.GetSubCategoryBySlug(slug);

            Assert.IsNotNull(result);
            Assert.AreEqual(slug, result.UrlSlug);
        }

        [TestCase("slug3-3")]
        public void Unable_To_Get_Subcategory_By_Slug(string slug)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns((SubCategory) null);

            var result = _service.GetSubCategoryBySlug(slug);

            Assert.IsNull(result);
        }

        [TestCase("Sub 1")]
        public void Subcategory_Is_Available(string name)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_subCategories);

            var result = _service.IsThisSubcategoryAvailable(name);

            Assert.IsTrue(result);
        }

        [TestCase("Sub 22")]
        public void Subcategory_Is_Unavailable(string name)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_emptyCategories);

            var result = _service.IsThisSubcategoryAvailable(name);

            Assert.IsFalse(result);
        }
       
        [TestCase(1)]
        public void Are_Subcategories_Of_Category_Deleted(int categoryId)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_subCategories);

            var result = _service.AreSubcategoriesOfCategoryDeleted(categoryId);

            Assert.IsTrue(result);
        }

        [TestCase(9)]
        public void Are_Subcategories_Of_Category_Not_Removed(int categoryId)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_emptyCategories);

            var result = _service.AreSubcategoriesOfCategoryDeleted(categoryId);

            Assert.IsFalse(result);
        }
        
        [Test]
        public void Create_Subcategory()
        {
            var subcategory = Mapper.Map<SubCategory, SubCategoryDTO>(_subCategories.First());

            _service.Create(subcategory);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Update_Subcategory()
        {
            var subcategory = Mapper.Map<SubCategory, SubCategoryDTO>(_subCategories.First());

            _service.Update(subcategory);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Delete_Category()
        {
            var subcategory = Mapper.Map<SubCategory, SubCategoryDTO>(_subCategories.First());

            _service.Delete(subcategory);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Get_All_Category()
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_subCategories);

            var result = _service.GetAll();

            Assert.IsNotNull(result);
            Assert.AreEqual(_subCategories.Count, result.Count());
        }

        [TestCase(4)]
        public void Get_Subcategories_By_Category(int categoryId)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_subCategories);

            var result = _service.GetSubCategoriesByCategory(categoryId);

            Assert.IsNotNull(result);
            Assert.AreEqual(_subCategories.Count, result.Count());
            Assert.AreEqual(categoryId, _subCategories.First().CategoryId);
        }

        [TestCase(3)]
        public void Get_No_Subcategory_By_Category(int categoryId)
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<SubCategory, bool>>>())).Returns(_emptyCategories);

            var result = _service.GetSubCategoriesByCategory(categoryId);

            Assert.IsNotNull(result);
            Assert.AreEqual(_emptyCategories.Count, result.Count());
        }
    }
}
