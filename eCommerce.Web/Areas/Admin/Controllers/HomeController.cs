﻿using System.Web.Mvc;
using eCommerce.Web.Controllers;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : BaseController
    {       
        // GET: Admin/Home
        public ActionResult Index(string sortOrder)
        {
            return View();
        }       
    }
}