﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Schema.Entity;

namespace eCommerce.UnitTest.Utilities
{
    public class AutoMappingRegistration
    {
        public static void Register()
        {
#pragma warning disable 618
            Mapper.CreateMap<User, UserDTO>().ReverseMap();
            Mapper.CreateMap<Role, RoleDTO>().ReverseMap();
            Mapper.CreateMap<Category, CategoryDTO>().ReverseMap();
            Mapper.CreateMap<SubCategory, SubCategoryDTO>().ReverseMap();
            Mapper.CreateMap<Order, OrderDTO>().ReverseMap();
            Mapper.CreateMap<ProductDTO, Product>().ReverseMap();
#pragma warning restore 618            
        }
    }
}
