﻿using System.Text;

namespace eCommerce.Ultility.StringHandler
{
    public class StringHandler
    {
        
        public static string CreateSlug(string text, string id = "")
        {            
            var slug = new StringBuilder(text.Trim());
            slug.Replace(',', '-');
            slug.Replace('&', '-');
            slug.Replace(' ', '-');
                        
            if (id != "" && id.Length >= 8)
            {
                slug.Append("-" + id.Substring(0, 8));
            }
            return slug.ToString();
        }
    }    
}
