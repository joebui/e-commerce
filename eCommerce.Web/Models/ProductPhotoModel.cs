﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ProductPhotoModel
    {
        public string Name { get; set; }
        public Guid ProductId { get; set; }
    }
}