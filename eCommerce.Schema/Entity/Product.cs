﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class Product : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Color? Color { get; set; }
        public Size? Size { get; set; }
        public int OrderCounter { get; set; }
        public int AvgRate { get; set; }
        public int Quantity { get; set; }
        public string RepresentImg { get; set; }
        public string UrlSlug { get; set; }
        public string Description { get; set; }
        public int SubCategoryId { get; set; }
        public int Status { get; set; }

        public virtual SubCategory SubCategory { get; set; }
        public virtual IList<ProductPhoto> ProductPhotos { get; set; }
        public virtual IList<ProductReview> ProductReviews { get; set; }
        public virtual IList<OrderDetail> OrderDetails { get; set; }
    }
}
