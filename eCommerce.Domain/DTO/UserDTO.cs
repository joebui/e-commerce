﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class UserDTO : BaseDTO<Guid>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int RoleId { get; set; }

        public virtual RoleDTO Role { get; set; }

        public virtual IList<OrderDTO>  Orders{ get; set; }
        public virtual IList<ProductReviewDTO> ProductReviews { get; set; }
    }
}
