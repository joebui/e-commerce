﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Framework.Infrastructure;
using eCommerce.Schema.Entity;

namespace eCommerce.Framework.Repository
{
    public interface IRoleRepository : IRepository<Role>
    {
        
    }

    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        public RoleRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
