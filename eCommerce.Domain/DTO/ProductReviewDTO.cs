﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class ProductReviewDTO : BaseDTO<Guid>
    {
        public string ReviewContent { get; set; }
        public int Rate { get; set; }

        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }

        public virtual UserDTO User { get; set; }
        public virtual ProductDTO Product { get; set; }
    }
}
