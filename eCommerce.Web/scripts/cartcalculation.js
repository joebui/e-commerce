﻿function calculateGrandTotal() {
    var total = 0.00;
    var items = document.getElementsByClassName("cart_total_price");
    for (var x = 0; x < items.length; x++) {
        var temp = items[x].innerHTML;
        total += parseFloat(temp.replace("$", ""));
    }
    document.getElementById("grand_total").innerHTML = "$" + total.toFixed(2);
    if (document.getElementById("cart-table").offsetHeight == 0 && document.getElementById("empty-cart-message") == null) {
        window.location.reload();
    }
}


function increaseQuantity(ItemId) {
    var x = document.getElementById("cart_quantity_input_" + ItemId);
    var price = document.getElementById("price_" + ItemId);
    var total_price = document.getElementById("cart_total_price_" + ItemId);
    x.value = parseInt(x.value) + 1;
    total_price.innerHTML = "$" + parseFloat(parseFloat(price.innerHTML.replace("$", "0")) * parseFloat(x.value)).toFixed(2);
    document.getElementById("total_products").innerHTML = parseInt(document.getElementById("total_products").innerHTML) + 1;
    calculateGrandTotal();
   
}

function decreaseQuantity(ItemId) {
    var x = document.getElementById("cart_quantity_input_" + ItemId);
    var price = document.getElementById("price_" + ItemId);
    var total_price = document.getElementById("cart_total_price_" + ItemId);
    if (x.value > 1) {
        x.value = parseInt(x.value) - 1;
        document.getElementById("total_products").innerHTML = parseInt(document.getElementById("total_products").innerHTML) - 1;
    }
    total_price.innerHTML = "$" + parseFloat(parseFloat(price.innerHTML.replace("$", "0")) * parseFloat(x.value)).toFixed(2);
    calculateGrandTotal();
    
}

function deleteItem(ItemId) {
    var count = parseInt(document.getElementById("cart_quantity_input_" + ItemId).value);
    var x = document.getElementById(ItemId);
    x.parentNode.removeChild(x);
    document.getElementById("total_products").innerHTML = parseInt(document.getElementById("total_products").innerHTML) - count;
    calculateGrandTotal();
}

function updateProductCount() {
    document.getElementById("total_products").innerHTML = parseInt(document.getElementById("total_products").innerHTML) + 1;
}


window.onload = calculateGrandTotal;

