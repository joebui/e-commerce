﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class ReviewFormViewModel
    {
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
        public string ReviewContent { get; set; }
        public int Rate { get; set; }
    }
}