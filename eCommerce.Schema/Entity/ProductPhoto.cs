﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class ProductPhoto : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public Guid ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
