﻿using eCommerce.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.IService
{
    public interface IProductPhotoService
    {
        IEnumerable<ProductPhotoDTO> GetProductPhoto(Guid productId);
    }
}
