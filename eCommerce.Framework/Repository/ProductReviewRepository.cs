﻿using eCommerce.Framework.Infrastructure;
using eCommerce.Schema.Entity;

namespace eCommerce.Framework.Repository
{
    public interface IProductReviewRepository : IRepository<ProductReview> { }
    public class ProductReviewRepository : RepositoryBase<ProductReview>, IProductReviewRepository
    {
        public ProductReviewRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
