﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eCommerce.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            

            routes.MapRoute(
                name: "UserManage",
                url: "{controller}/{action}/{username}",
                defaults: new { controller = "Auth", action = "Manage" },
                namespaces: new[] { "eCommerce.Web.Controllers" }
            );

            routes.MapRoute(
                name: "ProductSlug",
                url: "Products/{slug}",
                defaults: new { controller = "Product", action = "Detail" },
                namespaces: new[] { "eCommerce.Web.Controllers" }
            );

            routes.MapRoute(
                name: "ProductFilter",
                url: "product/category/{categoryFilter}/{sortOrder}",
                defaults: new { controller = "Product", action = "Index" },
                namespaces: new[] { "eCommerce.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "eCommerce.Web.Controllers" }
            );
        }
    }
}
