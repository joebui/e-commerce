﻿
using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class OrderManageViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal Total { get; set; }
        public int Quantity { get; set; }
        public OrderState State { get; set; }
        public IList<OrderDetailViewModel> OrderDetails { get; set; }
        public UserViewModel User { get; set; }
    }
}