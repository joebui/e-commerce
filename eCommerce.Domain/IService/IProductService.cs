﻿using eCommerce.Domain.DTO;
using System;
using System.Collections.Generic;
using eCommerce.Ultility.Type;

namespace eCommerce.Domain.IService
{
    public interface IProductService
    {
        IEnumerable<ProductDTO> GetAllProductsPublished();
        IEnumerable<ProductDTO> GetProductsBySubCategorySlug(string subCategoryName);        
        IEnumerable<ProductDTO> GetRecommendedProduct();
        IEnumerable<ProductDTO> GetProductByName(string key);
        IEnumerable<ProductDTO> GetAllAdmin();        
        IEnumerable<ProductDTO> GetProductsWithStatus(string state);        

        ProductDTO GetBySlug(string slug);
        ProductDTO GetById(Guid id);

        int CountAvailableProductsBySubcategory(int subId);

        void UpdateRating(Guid productId);
        void Create(ProductDTO productDTO);
        void Delete(Guid productId, Guid adminId);
        void Save();
        void Approve(Guid productId, Guid adminId);
        void UpdateProduct(ProductDTO productDTO);
    }
}
