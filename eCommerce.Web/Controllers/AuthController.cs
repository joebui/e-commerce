﻿using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Ultility.PasswordHandler;
using eCommerce.Ultility.Type;
using eCommerce.Web.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace eCommerce.Web.Controllers
{
    public class AuthController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IOrderService _orderService;

        public AuthController(IUserService userService, IRoleService roleService, IOrderService orderService)
        {
            _userService = userService;
            _roleService = roleService;
            _orderService = orderService;
        }

        public AuthController()
        {            
        }

        // GET: Auth/Register
        [AllowAnonymous]
        public ActionResult Register()
        {            
            return View("Register");
        }

        // POST: Auth/Register
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserName,Email,Password,RetypePassword,Address,PhoneNumber")] RegisterModel model)
        {
            if (!ModelState.IsValid) return View("Register", model);

            model.UserName = model.UserName.Replace(" ", "");
            if (_userService.CheckRegisterUsernameEmail(model.UserName, model.Email) != null)
            {
                ModelState.AddModelError("available", "Username/Email is already taken");
                return View("Register", model);
            }

            model.Id = Guid.NewGuid();
            model.Password = Encryption.Encrypt(model.Password);            
            model.RoleId = _roleService.GetRoleByName(RoleType.User.ToString()).Id;

            var userDto = Mapper.Map<RegisterModel, UserDTO>(model);            
            _userService.AddNewUser(userDto);
            LoginAccount(model.Id, model.UserName, RoleType.User.ToString());

            return RedirectToAction("Index", "Home");
        }

        // GET: Auth/Login        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: Auth/Login
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid) return View("Login", model);

            var user = _userService.CheckLoginUsernamEmail(model.UsernameEmail);
            if (user != null)
            {
                var decryptedPassword = Decryption.Decrypt(user.Password);
                if (model.Password == decryptedPassword && !user.IsDeleted)
                {                    
                    LoginAccount(user.Id, user.UserName, user.Role.Name);
                    return RedirectToLocal(returnUrl);
                }
            }

            ModelState.AddModelError("userpass", "Username/password is incorrect.");
            return View(model);
        }

        // GET: Auth/Manage
        [Authorize]
        public ActionResult Manage(string username,int page=1)
        {
            if (username == null) return Redirect("~/ErrorPages/404.html");

            if (User.Identity.Name != username) return RedirectToAction("Index", "Home");            
            var user = _userService.FindUserByName(username);            

            var ordersDto = _orderService.GetOrderByUserId(user.Id).Take(10);            
            var order = Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(ordersDto.ToList());
            var data = new ManageUserAndOrder { RegisterModel = Mapper.Map<UserDTO, RegisterModel>(user), OrderManageViewModel = order };

            return View("Manage", data);
        }

        // POST: Auth/Manage
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ManageUserAndOrder model, string name, string email)
        {
            FillRecentOrders(model, name);
            if (!ModelState.IsValid) return View("Manage", model);


            if (_userService.CheckRegisterUsernameEmail(model.RegisterModel.UserName, model.RegisterModel.Email) != null &&
                model.RegisterModel.UserName != name && model.RegisterModel.Email != email)
            {
                ModelState.AddModelError("available", "Username/Email is already taken");
                return View("Manage", model);
            }

            model.RegisterModel.UserName = model.RegisterModel.UserName.Replace(" ", "");
            model.RegisterModel.Password = Encryption.Encrypt(model.RegisterModel.Password);
            var user = Mapper.Map<RegisterModel, UserDTO>(model.RegisterModel);
            _userService.UpdateUser(user);

            var role = _roleService.GetRoleById(model.RegisterModel.RoleId);
            LoginAccount(model.RegisterModel.Id, model.RegisterModel.UserName, role.Name);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();

            return RedirectToAction("Index","Home");
        }        

        #region AuthUtilities        
        private void LoginAccount(Guid id, string name, string role)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.Role, role),
                new Claim(ClaimTypes.NameIdentifier, id.ToString())
            }, DefaultAuthenticationTypes.ApplicationCookie);

            var authManager = Request.GetOwinContext().Authentication;
            authManager.SignIn(identity);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public ManageUserAndOrder FillRecentOrders(ManageUserAndOrder data, string username)
        {
            var user = _userService.FindUserByName(username);
            var ordersDto = _orderService.GetOrderByUserId(user.Id).Take(10);            
            var order = Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(ordersDto.ToList());
            data.OrderManageViewModel = order;
            return data;
        }
        #endregion
    }
}