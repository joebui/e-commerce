﻿$(document).ready(function () {
    var url = $('#url_cancelOrder').val();
    $('#btn_cancel').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modalCancelOrder').data('id', id).modal('show');
    })
    $('#btnConfirmCancel').click(function () {
        var id = $('#modalCancelOrder').data('id');
        $.ajax({
            url: url,
            data: { 'orderId': id },
            type: 'GET',
            success: function () {
                $('#order_state').html('<label>State: </label><span class="label label-danger">Canceled</span>');
                $('#btn_cancel').hide();
                $('#btn_approve').hide();
            },
            error: function () {
                alert("failed");
            }
        })
        $('#modalCancelOrder').modal('hide');
    })
})