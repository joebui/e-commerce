﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using eCommerce.Ultility.EmailHandler;
using eCommerce.Web.Controllers;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: Admin/Order
        public ActionResult Index(int page=1)
        {
            ViewBag.Search = new SelectList(new List<string>() {"All", "Pending", "Approve" });
            var ordersDto = _orderService.GetAll().OrderByDescending(o => o.CreatedDate);
            var ordersVm = Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(ordersDto.ToList());

            return View(ordersVm.ToPagedList(page,10));
        }

        // GET: Admin/Order/Detail
        public ActionResult Detail(Guid orderId)
        {
            var orderDto = _orderService.GetOrderById(orderId);
            var orderVm = Mapper.Map<OrderDTO, OrderManageViewModel>(orderDto);

            return View(orderVm);
        }        

        public async Task<ActionResult> ApproveOrder(Guid orderId)
        {
            var email = _orderService.ApproveOrder(orderId);
            await OrderApproveConfirmation.Send(email, orderId.ToString());

            return RedirectToAction("Index");
        }

        public ActionResult DeleteOrder(Guid id)
        {
            _orderService.DeleteOrder(id);
            return RedirectToAction("Index");
        }

        public PartialViewResult SearchByUser(string result)
        {
            var orders = _orderService.GetOrderByUsername(result);
            return PartialView("_SearchResult", Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(orders.ToList()));            
        }

        public PartialViewResult SearchByState(string search)
        {
            if (search == "All")
            {
                var ordersDto = _orderService.GetAll().OrderByDescending(o => o.CreatedDate);
                var ordersVm = Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(ordersDto.ToList());

                return PartialView("_SearchResult", ordersVm);
            }
            else
            {
                var orders = _orderService.GetOrderByState(search);
                return PartialView("_SearchResult", Mapper.Map<List<OrderDTO>, List<OrderManageViewModel>>(orders.ToList()));
            }            
        }

        #region OrderFunctions
        public void CancelOrder(Guid orderId)
        {
            _orderService.CancelOrder(orderId);
        }
        #endregion
    }
}