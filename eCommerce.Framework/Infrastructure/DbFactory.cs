﻿using eCommerce.Framework.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Framework.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        ECommerceContext dbContext;

        public ECommerceContext Init()
        {
            return dbContext ?? (dbContext = new ECommerceContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
