﻿$(document).ready(function () {
    var url = $("#url").val();
    $(".delete-sub").on("click", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var category = $(this).data("category");
        $("#modalDeleteSub").data("id", id).data("category", category).modal("show");
    });

    $("#btnConfirmDelete").click(function () {
        var id = $("#modalDeleteSub").data("id");
        var category = $("#modalDeleteSub").data("category");
        $.ajax({
            type: "POST",
            url: url,
            data: { 'id': id, 'category': category },
            dataType: "json",
            success: function (response) {
                window.location = response.Url;                
            },
            error: function (response) {
                alert("Failed");
            }
        });

        $("#modalDeleteSub").modal("hide");
    });
})