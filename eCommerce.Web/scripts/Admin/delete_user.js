﻿$(document).ready(function () {
    var url = $("#url_delete_user").val();
    $('body').on("click", ".delete-user", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        $("#modalDeleteUser").data("id", id).modal("show");        
    })

    $("#btnConfirmDelete").click(function () {
        var id = $("#modalDeleteUser").data("id");        
        $.ajax({
            url: url,
            data: { 'id': id },
            type: "POST",
            dataType: "json",
            success: function (response) {
                window.location = response.Url;                
            },
            error: function () {
                alert("failed");
            }
        });
        $("#modalDeleteUser").modal("hide");
    })
})