﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Ultility;
using eCommerce.Ultility.StringHandler;
using eCommerce.Web.Areas.Admin.Models;
using eCommerce.Web.Controllers;
using PagedList;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SubcategoryController : BaseController
    {
        private readonly ISubCategoryService _subCategoryService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;

        public SubcategoryController(ISubCategoryService subCategoryService, ICategoryService categoryService, IProductService productService)
        {
            _subCategoryService = subCategoryService;
            _categoryService = categoryService;
            _productService = productService;
        }

        // GET: Admin/Subcategory
        public ActionResult Index(int id, int page = 1)
        {
            ViewBag.Category = _categoryService.GetCategoryById(id).Name;
            var subCategory = _subCategoryService.GetSubCategoriesByCategory(id);
            return View(subCategory.OrderBy(s => s.Name).ToPagedList(page, 10));
        }

        // GET: Admin/Subcategory/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name");
            return View();
        }

        // POST: Admin/Subcategory/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,CategoryId")] SubcategoryModel model)
        {
            if (ModelState.IsValid)
            {
                if (_subCategoryService.IsThisSubcategoryAvailable(model.Name))
                {
                    ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name");
                    ModelState.AddModelError("Name", "This subcategory is already available.");
                    return View(model);
                }

                model.UrlSlug = StringHandler.CreateSlug(model.Name, model.Id.ToString());
                _subCategoryService.Create(Mapper.Map<SubcategoryModel, SubCategoryDTO>(model));
                return RedirectToAction("Index", "Subcategory", new { id = model.CategoryId });
            }

            ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name");
            return View(model);            
        }

        // GET: Admin/Subcategory/Edit
        public ActionResult Edit(int id)
        {
            var sub = _subCategoryService.GetSubCategoryById(id);
            ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name", sub.CategoryId);
            return View(Mapper.Map<SubCategoryDTO, SubcategoryModel>(sub));
        }

        // POST: Admin/Subcategory/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CategoryId")] SubcategoryModel model)
        {
            if (ModelState.IsValid)
            {
                if (_subCategoryService.IsThisSubcategoryAvailable(model.Name))
                {
                    ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name", model.CategoryId);
                    ModelState.AddModelError("Name", "This subcategory is already available.");
                    return View(model);
                }

                model.UrlSlug = StringHandler.CreateSlug(model.Name, model.Id.ToString());
                _subCategoryService.Update(Mapper.Map<SubcategoryModel, SubCategoryDTO>(model));
                return RedirectToAction("Index", "Subcategory", new { id = model.CategoryId });                
            }

            ViewBag.CategoryId = new SelectList(_categoryService.GetAllCategory(), "Id", "Name", model.CategoryId);
            return View(model);
        }

        public ActionResult Delete(int id, int category)
        {
            if (_productService.CountAvailableProductsBySubcategory(id) != 0)
                return Json(new {Url = Url.Action("ActionDenied")});

            _subCategoryService.Delete(_subCategoryService.GetSubCategoryById(id));
            return Json( new { Url = Url.Action("Index", "Subcategory", new { id = category }) } );
        }

        // GET: Admin/Subcategory/ActionDenied
        public ActionResult ActionDenied()
        {
            return View();
        }
    }
}