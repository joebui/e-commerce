﻿using eCommerce.Domain.DTO;
using System;
using System.Collections.Generic;

namespace eCommerce.Domain.IService
{
    public interface IOrderService
    {
        IEnumerable<OrderDTO> GetAll();
        IEnumerable<OrderDetailDTO> GetOrderDetail(Guid orderId);
        IEnumerable<OrderDTO> GetOrderByUserId(Guid userId);
        IEnumerable<OrderDTO> GetOrderByUsername(string name);
        IEnumerable<OrderDTO> GetOrderByState(string state);

        OrderDTO GetOrderById(Guid orderId);

        Guid Create(Guid userId, int total, IEnumerable<OrderDetailDTO> orderDetails);
        decimal CalculateTotalPrice(Guid orderId);
        int CalculateTotalProductQuantity(Guid orderId);        
        string ApproveOrder(Guid orderId);
        int CountAvailableOrdersByUserId(Guid userId);

        void DeleteOrder(Guid orderId);
        void CancelOrder(Guid orderId);           
    }
}
