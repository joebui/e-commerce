﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class Order : BaseEntity<Guid>
    {
        public decimal Total { get; set; }
        public int Quantity { get; set; }
        public int State { get; set; }

        public Guid UserId { get; set; }

        public virtual IList<OrderDetail> OrderDetails { get; set; }
        public virtual User User { get; set; }
    }
}
