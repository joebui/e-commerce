﻿using Autofac;
using Autofac.Integration.Mvc;
using eCommerce.Domain.IService;
using eCommerce.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using eCommerce.Service.Service;

namespace eCommerce.Web.Library
{
    public class IocConfig
    {
        public static void Run()
        {
            SetAutofacContainer();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            Framework.IocRegister.Instance.Configure(builder);
            Service.IocRegister.Instance.Configure(builder);

            builder.RegisterAssemblyTypes(typeof(CategoryService).Assembly)
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces().InstancePerRequest();

            AutoMappingConfiguration.Instance.Configure();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}