﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class CategoryDTO : BaseDTO<int>
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }

        public virtual IList<SubCategoryDTO> SubCategories { get; set; }
    }
}
