﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Domain.DTO;

namespace eCommerce.Domain.IService
{
    public interface IProductReviewService
    {
        IEnumerable<ProductReviewDTO> LoadReviews(Guid productId);

        void AddReview(ProductReviewDTO reviewDTO);        
        void Save();
    }
}
