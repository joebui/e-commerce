﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Ultility.Type
{
    public enum Color
    {
        Red,
        Yellow,
        White,
        Blue,
        Purple,
        Green,
        Pink,
        Black,
        Brown,
        Orange
    }
}
