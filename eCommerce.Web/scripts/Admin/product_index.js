﻿$('body').on('click','.delete-product', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $('#modalDeleteProduct').data('id', id).modal('show');
})

$('#btnConfirmDelete').click(function () {
    var id = $('#modalDeleteProduct').data('id');
    $.ajax({
        url: '/Admin/Product/Delete',
        data: { 'productId': id },
        type: 'GET',
        success: function () {
            $('#modalDeleteProduct').modal('hide');
            $('a[data-id="' + id + '"]').closest('tr').hide(500);
        },
        error: function () {
            alert("failed");
        }
    })
})

$('body').on('click', '.approve-product', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $('#modalApproveProduct').data('id', id).modal('show');
})

$('#btnConfirmApprove').click(function () {
    var id = $('#modalApproveProduct').data('id');
    var item = $('a[data-id="' + id + '"][class="btn btn-primary approve-product"]');
    $.ajax({
        url: '/Admin/Product/Approve',
        data: { 'productId': id },
        type: 'GET',
        success: function () {
            $('#modalApproveProduct').modal('hide');
            item.parent().html('<h4><p class="text-success">Online</p></h4>');
        },
        error: function () {
            alert("failed");
        }
    })
})