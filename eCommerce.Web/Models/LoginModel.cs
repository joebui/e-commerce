﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Models
{
    public class LoginModel
    {   
        [Required]     
        public virtual string UsernameEmail { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string Password { get; set; }
    }
}