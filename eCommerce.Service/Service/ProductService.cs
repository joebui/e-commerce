﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Ultility;
using eCommerce.Ultility.StringHandler;
using eCommerce.Ultility.Type;

namespace eCommerce.Service.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductReviewRepository _reviewRepository;
        private readonly IProductPhotoRepository _photoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IProductRepository productRepository,IProductPhotoRepository photoRepository, IUnitOfWork unitOfWork, IProductReviewRepository reviewRepository)
        {
            _productRepository = productRepository;
            _photoRepository = photoRepository;
            _unitOfWork = unitOfWork;            
            _reviewRepository = reviewRepository;
        }

        public void Create(ProductDTO productDTO)
        {
            var product = PrepareProduct(productDTO);
            if (productDTO.Images.Any(p => p != null))
            {
                product.RepresentImg = product.Id.ToString() + ".jpg";             
                AddImages(productDTO.Images, product.Id);
            }
            _productRepository.Add(product);
            _unitOfWork.Commit();
        }

        private Product PrepareProduct(ProductDTO productDTO)
        {
            var product = Mapper.Map<ProductDTO, Product>(productDTO);
            product.Id = Guid.NewGuid();
            product.UrlSlug = StringHandler.CreateSlug(product.Name, product.Id.ToString());
            product.Status = 0;
            product.CreatedDate = DateTime.Now;
            return product;
        }

        private void AddImages(List<HttpPostedFileBase> images, Guid productId)
        {
            var hasRepresentImg = false;
            var name = productId.ToString();
            var count = 0;
            for (var i = 0; i < images.Count; i++)
            {
                if(images[i] == null)
                    continue;

                if (!hasRepresentImg)
                {
                    ImageHandler.SavePhoto(images[i], name, "represent_large");
                    ImageHandler.SavePhoto(images[i], name, "represent_small");
                    ImageHandler.SavePhoto(images[i], name, "represent_landscape");
                    hasRepresentImg = true;
                }

                var tempName = productId.ToString() + "_" + count;
                count++;
                ImageHandler.SavePhoto(images[i], tempName, "slide_main");
                ImageHandler.SavePhoto(images[i], tempName, "slide_thumbnail");
                AddImageToDB(productId, tempName);
            }
        }

        private void AddImageToDB(Guid productId, string name)
        {
            var photo = new ProductPhoto { Id = Guid.NewGuid(), Name = name + ".jpg", ProductId = productId };
            _photoRepository.Add(photo);
        }        

        public ProductDTO GetBySlug(string slug)
        {
            var product = _productRepository.Get(x => x.UrlSlug == slug && x.Status==1 && !x.IsDeleted);

            if(product != null)
            {
                var productDto = Mapper.Map<Product, ProductDTO>(product);
                productDto.ProductReviews.OrderByDescending(r => r.CreatedDate);
                return productDto;
            }

            return null;
        }

        public int CountAvailableProductsBySubcategory(int subId)
        {            
            return _productRepository.GetMany(s => s.SubCategoryId == subId).Count(s => !s.IsDeleted);
        }

        public void UpdateRating(Guid productId)
        {
            var reviews = _reviewRepository.GetMany(r => r.ProductId == productId);
            var rate = (int)reviews.Average(r => r.Rate);
            var product = _productRepository.GetById(productId);
            product.AvgRate = rate;
            _productRepository.Update(product);
        }

        public void Approve(Guid productId, Guid adminId)
        {
            var product = _productRepository.GetById(productId);
            product.Status = 1;
            product.UpdatedDate = DateTime.Now;
            product.UpdatedBy = adminId;
            _productRepository.Update(product);
        }

        public void Delete(Guid productId, Guid adminId)
        {
            var product = _productRepository.GetById(productId);
            product.IsDeleted = true;
            product.DeletedDate = DateTime.Now;
            product.DeletedBy = adminId;
            _productRepository.Update(product);
        }

        public IEnumerable<ProductDTO> GetAllAdmin()
        {
            var products = _productRepository.GetMany(p => !p.IsDeleted).OrderByDescending(p => p.CreatedDate);            
            return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList());
        }

        public IEnumerable<ProductDTO> GetProductsWithStatus(string state)
        {
            if (state == ProductState.Pending.ToString())
            {
                var products = _productRepository.GetMany(p => p.Status == (int) ProductState.Pending && !p.IsDeleted);
                return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList());
            }
            else
            {
                var products = _productRepository.GetMany(p => p.Status == (int) ProductState.Approved && !p.IsDeleted);
                return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList());
            }           
        }

        public IEnumerable<ProductDTO> GetAllProductsPublished()
        {
            var products = _productRepository.GetMany(p => !p.IsDeleted && p.Status==1);            
            return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList()); ;
        }

        public ProductDTO GetById(Guid id)
        {
            var product = _productRepository.GetById(id);            
            return Mapper.Map<Product, ProductDTO>(product);
        }        

        public IEnumerable<ProductDTO> GetProductsBySubCategorySlug(string subCategorySlug)
        {
            var products = _productRepository.GetMany(p => p.SubCategory.UrlSlug==subCategorySlug && 
                p.Status==1 && p.IsDeleted==false);            
            return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList()); ;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<ProductDTO> GetRecommendedProduct()
        {
            var products = _productRepository.GetMany(x=> x.Status == 1 && !x.IsDeleted).OrderByDescending(p => p.OrderCounter).Take(9);            
            return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList()); ;
        }

        public IEnumerable<ProductDTO> GetProductByName(string key)
        {
            var products = _productRepository.GetMany(p=>p.Name.Contains(key) && !p.IsDeleted);           
            return Mapper.Map<List<Product>, List<ProductDTO>>(products.ToList()); ;
        }

        public void UpdateProduct(ProductDTO productDTO)
        {
            var product = _productRepository.GetById(productDTO.Id);
            product.Name = productDTO.Name;
            product.Price = productDTO.Price;
            product.Description = productDTO.Description;
            product.SubCategoryId = productDTO.SubCategoryId;
            product.UrlSlug = StringHandler.CreateSlug(product.Name, product.Id.ToString());
            product.UpdatedDate = DateTime.Now;
            _productRepository.Update(product);
            Save();
        }
    }
}
