﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Ultility.EmailHandler
{
    public class CheckOutConfirmation
    {
        public static Task Send(string email, List<string> cart)
        {
            var message = new MailMessage();            
            message.To.Add(new MailAddress(email));            

            message.Subject = "[DoNotReply] Gift4Charity - Confirmation of your checkout orders";
            message.Body = BodyContent(cart);
            message.IsBodyHtml = true;

            var smtp = new SmtpClient();
            return smtp.SendMailAsync(message);            
        }

        public static string BodyContent(List<string> cart)
        {
            var builder = new StringBuilder();
            builder.Append("<h1> You have just checkout the following items: </h1>");
            builder.Append("<hr/>");
            foreach (var item in cart)
            {
                builder.Append("<h4>");
                builder.Append(item);
                builder.Append("</h4>");
            }

            return builder.ToString();
        }
    }
}
