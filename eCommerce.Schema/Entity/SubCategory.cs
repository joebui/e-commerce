﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class SubCategory : BaseEntity<int>
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public int CategoryId { get; set; }

        public virtual IList<Product> Products { get; set; }
        public virtual Category Category { get; set; }
    }
}
