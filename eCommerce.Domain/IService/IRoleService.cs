﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Domain.DTO;

namespace eCommerce.Domain.IService
{
    public interface IRoleService
    {
        RoleDTO GetRoleByName(string name);
        RoleDTO GetRoleById(int id);

        IEnumerable<RoleDTO> GetAllRoles();
    }
}
