﻿using Autofac;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Framework
{
    public class IocRegister
    {
        public void Configure(ContainerBuilder container)
        {
            container.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            container.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
        }

        #region Singletons

        private static readonly Lazy<IocRegister> LazyInstance = new Lazy<IocRegister>(() => new IocRegister());

        public static IocRegister Instance
        {
            get { return LazyInstance.Value; }
        }

        #endregion
    }
}
