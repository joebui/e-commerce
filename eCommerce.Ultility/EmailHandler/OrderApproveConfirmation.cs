﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Ultility.EmailHandler
{
    public class OrderApproveConfirmation
    {
        public static Task Send(string email, string orderId)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(email));

            message.Subject = "[DoNotReply] Gift4Charity - Confirmation of your checkout orders";
            message.Body = BodyContent(orderId);
            message.IsBodyHtml = true;

            var smtp = new SmtpClient();
            return smtp.SendMailAsync(message);
        }

        public static string BodyContent(string orderId)
        {
            var builder = new StringBuilder();
            builder.Append("<h1> Your order has just been processed </h1>");
            builder.Append("<br/>");
            builder.Append("<h4> Order ID: ");
            builder.Append(orderId);
            builder.Append("</h4>");
            builder.Append(
                "<p><b> The items will be delivered to your address within 3 days. Thank you for shopping at Gift4Charity </b></p>");

            return builder.ToString();
        }
    }
}
