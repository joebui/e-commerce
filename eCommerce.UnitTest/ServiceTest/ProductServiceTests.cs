﻿using System;
using System.Collections.Generic;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;
using eCommerce.Domain.DTO;
using eCommerce.Service.Service;
using System.Web;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Linq.Expressions;

namespace eCommerce.UnitTest.ServiceTest
{
    class ProductServiceTests
    {
        private Mock<IProductRepository> _repoProduct;
        private Mock<IProductPhotoRepository> _repoPhoto;
        private Mock<IProductReviewRepository> _repoReview;
        private Mock<IOrderRepository> _repoOrder;
        private IProductService _service;
        private Mock<IUnitOfWork> _unit;
        private readonly List<Product> _products = new List<Product>();
        private FileStream _stream;

        [SetUp]
        public void Setup()
        {
            _repoProduct = new Mock<IProductRepository>();
            _repoReview = new Mock<IProductReviewRepository>();
            _repoPhoto = new Mock<IProductPhotoRepository>();
            _repoOrder = new Mock<IOrderRepository>();
            _unit = new Mock<IUnitOfWork>();
            _service = new ProductService(_repoProduct.Object, _repoPhoto.Object, _unit.Object, _repoReview.Object);
            AutoMappingRegistration.Register();

            _products.Add(new Product() { Id = Guid.NewGuid(), Name = "Prod 1", Description = "description 1", UrlSlug = "Prod-1" });
            _products.Add(new Product() { Id = Guid.NewGuid(), Name = "Prod 2", Description = "description 2", UrlSlug = "Prod-2" });
            _products.Add(new Product() { Id = Guid.NewGuid(), Name = "Prod 3", Description = "description 3", UrlSlug = "Prod-3" });
            _products.Add(new Product() { Id = Guid.NewGuid(), Name = "Prod 4", Description = "description 4", UrlSlug = "Prod-4" });
        }

         //Write your service tests from here.
        [Test]
        public void Create_Product()
        {
            var id = Guid.NewGuid();
            var files = new List<HttpPostedFileBase>();
            var product = new ProductDTO { Id = id, Name = "Name", Description = "Description", Images = new List<HttpPostedFileBase>() };
            _service.Create(product);

            var result = _service.GetById(id);
            _unit.Verify(u => u.Commit(), Times.Once);       
        }

        [Test] 
        public void GetAnExistedProductBySlug_ShouldReturnProduct()
        {
            var product = _products.First();
            _repoProduct.Setup(r => r.Get(It.IsAny<Expression<Func<Product, bool>>>())).Returns(product);

            var result = _service.GetBySlug("Prod-1");

            Assert.IsNotNull(result);
            Assert.AreEqual(product.Name,result.Name);
        }

        [Test]
        public void ApproveAnPendingProduct_ShouldChangeProductStateToApproved()
        {
            var product = _products.First();
            _repoProduct.Setup(r => r.GetById(It.IsAny<Guid>())).Returns(product);

            _service.Approve(product.Id,Guid.NewGuid());

            Assert.AreEqual(1, product.Status);
        }

        [Test]
        public void DeleteAnOrder_ShouldMarkThatOrderAsDeleted()
        {
            var product = _products.First();
            _repoProduct.Setup(r => r.GetById(It.IsAny<Guid>())).Returns(product);

            _service.Delete(product.Id,Guid.NewGuid());

            Assert.IsTrue(product.IsDeleted);
        }

        [Test]
        public void GetProductById_NotFound_ShouldReturnNull()
        {
            _repoProduct.Setup(r => r.GetById(It.IsAny<Guid>())).Returns((Product)null);

            var result = _service.GetById(Guid.NewGuid()) as ProductDTO;

            Assert.IsNull(result);
        }

        [Test]
        public void GetOrderById_Found_ShouldReturnOrder()
        {
            var product = _products.First();
            _repoProduct.Setup(r => r.GetById(product.Id)).Returns(product);

            var result = _service.GetById(product.Id) as ProductDTO;

            Assert.IsNotNull(result);
            Assert.AreEqual(product.Quantity, result.Quantity);
        }

        [Test]
        public void GetAllProductPublished_ShouldReturnListMockProducts()
        {
            _repoProduct.Setup(r => r.GetMany(It.IsAny<Expression<Func<Product, bool>>>())).Returns(_products);

            var result = _service.GetAllProductsPublished() as List<ProductDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(_products.Count, result.Count);
        }
    }
}
