﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Service.Service;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;

namespace eCommerce.UnitTest.ServiceTest
{
    class UserServiceTests
    {
        private Mock<IUserRepository> _repo;
        private IUserService _service;
        private Mock<IUnitOfWork> _unit;
        private readonly List<User> _users = new List<User>();

        [SetUp]
        public void Setup()
        {
            _repo = new Mock<IUserRepository>();
            _unit = new Mock<IUnitOfWork>();
            _service = new UserService(_repo.Object, _unit.Object);
            AutoMappingRegistration.Register();

            _users.Add(new User() { Id = Guid.NewGuid(), UserName = "admin", Email = "admin@mail.com" });
            _users.Add(new User() { Id = Guid.NewGuid(), UserName = "dien", Email = "dien@mail.com" });
            _users.Add(new User() { Id = Guid.NewGuid(), UserName = "john", Email = "john@mail.com" });
            _users.Add(new User() { Id = Guid.NewGuid(), UserName = "charles", Email = "charles@mail.com" });
        }

        [TestCase("admin", "admin@mail.com")]
        public void Registered_Username_And_Email_Are_Available(string name, string email)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(_users.First());

            var result = _service.CheckRegisterUsernameEmail(name, email);

            Assert.IsNotNull(result);
            Assert.AreEqual(name, result.UserName);
            Assert.AreEqual(email, result.Email);
        }

        [TestCase("vinhvo", "vinh@mail.com")]
        public void Registered_Username_And_Email_Are_Unavailable(string name, string email)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var result = _service.CheckRegisterUsernameEmail(name, email);

            Assert.IsNull(result);
        }

        [TestCase("dien")]
        public void Successful_Login_By_Username(string username)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(_users[1]);

            var result = _service.CheckLoginUsernamEmail(username);

            Assert.IsNotNull(result);
            Assert.AreEqual(username, result.UserName);
        }

        [TestCase("harry")]
        public void Unsuccessful_Login_By_Username(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var result = _service.CheckLoginUsernamEmail(name);

            Assert.IsNull(result);
        }

        [TestCase("dien@mail.com")]
        public void Successful_Login_By_Email(string email)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(_users[1]);

            var result = _service.CheckLoginUsernamEmail(email);

            Assert.IsNotNull(result);
            Assert.AreEqual(email, result.Email);
        }

        [TestCase("harry@mail.com")]
        public void Unsuccessful_Login_By_Email(string email)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var result = _service.CheckLoginUsernamEmail(email);

            Assert.IsNull(result);
        }

        [TestCase("john")]
        public void Find_User_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(_users[2]);

            var result = _service.FindUserByName(name);

            Assert.IsNotNull(result);
            Assert.AreEqual(name, result.UserName);
        }

        [TestCase("kate")]
        public void Unable_To_Find_User_By_Name(string name)
        {
            _repo.Setup(r => r.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var result = _service.FindUserByName(name);

            Assert.IsNull(result);
        }

        [Test]
        public void Find_User_By_Id()
        {
            var user = _users.First();
            _repo.Setup(r => r.GetById(user.Id)).Returns(user);

            var result = _service.FindUserById(user.Id);

            Assert.IsNotNull(result);
            Assert.AreEqual(user.UserName, result.UserName);
        }

        [Test]
        public void Unable_To_Find_User_By_Id()
        {
            var user = _users.First();
            _repo.Setup(r => r.GetById(user.Id)).Returns((User)null);

            var result = _service.FindUserById(user.Id);

            Assert.IsNull(result);
        }

        [Test]
        public void Get_All_User()
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<User, bool>>>())).Returns(_users);

            var result = _service.GetAllUsers();

            Assert.IsNotNull(result);
            Assert.AreEqual(_users.Count, result.Count());
        }

        [Test]
        public void Add_New_User()
        {
            var user = Mapper.Map<User, UserDTO>(_users.First());

            _service.AddNewUser(user);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Update_User()
        {
            var user = Mapper.Map<User, UserDTO>(_users.First());

            _service.UpdateUser(user);

            _unit.Verify(u => u.Commit(), Times.Once);
        }

        [Test]
        public void Remove_User()
        {
            var user = Mapper.Map<User, UserDTO>(_users.First());

            _service.RemoveUser(user);

            _unit.Verify(u => u.Commit(), Times.Once);
        }
    }
}
