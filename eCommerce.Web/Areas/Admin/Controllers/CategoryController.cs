﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Areas.Admin.Models;
using eCommerce.Web.Controllers;
using PagedList;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;
        private readonly ISubCategoryService _subCategoryService;

        public CategoryController(ICategoryService categoryService, ISubCategoryService subCategoryService)
        {
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
        }

        // GET: Admin/Category
        public ActionResult Index(int page = 1)
        {
            var categories = _categoryService.GetAllCategory();
            return View(categories.OrderBy(c => c.Name).ToPagedList(page, 10));
        }

        // GET: Admin/Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] CategoryModel model)
        {
            if (!ModelState.IsValid) return View(model);

            if (_categoryService.IsThisCategoryAvailable(model.Name))
            {                    
                ModelState.AddModelError("Name", "This category is already available.");
                return View(model);
            }

            _categoryService.Create(Mapper.Map<CategoryModel, CategoryDTO>(model));

            // Create default subcategory for the new category.            
            var thisCategoryId = _categoryService.GetCategoryByName(model.Name).Id;
            _subCategoryService.Create(new SubCategoryDTO() { Name = model.Name, CategoryId = thisCategoryId});
            return RedirectToAction("Index", "Subcategory", new { id = thisCategoryId });
        }

        // GET: Admin/Category/Edit
        public ActionResult Edit(int id)
        {            
            var category = _categoryService.GetCategoryById(id);
            return View(Mapper.Map<CategoryDTO, CategoryModel>(category));
        }

        // POST: Admin/Category/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryModel model)
        {
            if (!ModelState.IsValid) return View(model);

            if (_categoryService.IsThisCategoryAvailable(model.Name))
            {
                ModelState.AddModelError("Name", "This category is already available.");
                return View(model);
            }

            var categoryDto = Mapper.Map<CategoryModel, CategoryDTO>(model);
            _categoryService.Update(categoryDto);
            return RedirectToAction("Index");
        }
        
        public ActionResult Delete(int id)
        {
            var subCategoryDeleted = _subCategoryService.AreSubcategoriesOfCategoryDeleted(id);

            if (!subCategoryDeleted) return Json(new { Url = Url.Action("ActionDenied") });

            _categoryService.Delete(_categoryService.GetCategoryById(id));                
            return Json(new { Url = Url.Action("Index") });
        }

        // GET: Admin/Category/ActionDenied
        public ActionResult ActionDenied()
        {
            return View();
        }
    }
}