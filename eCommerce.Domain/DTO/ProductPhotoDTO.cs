﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.DTO
{
    public class ProductPhotoDTO : BaseDTO<Guid>
    {
        public string Name { get; set; }
        public Guid ProductId { get; set; }

        public virtual ProductDTO Product { get; set; }
    }
}
