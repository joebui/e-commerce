﻿using eCommerce.Ultility.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace eCommerce.Domain.DTO
{
    public class ProductDTO : BaseDTO<Guid>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Color? Color { get; set; }
        public Size? Size { get; set; }
        public int OrderCounter { get; set; }
        public int AvgRate { get; set; }
        public int Quantity { get; set; }
        public string UrlSlug { get; set; }
        public string RepresentImg { get; set; }
        public string Description { get; set; }
        public int SubCategoryId { get; set; }
        public ProductState Status { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }

        public List<HttpPostedFileBase> Images { get; set; }
        public SubCategoryDTO SubCategory { get; set; }
        public IList<ProductPhotoDTO> ProductPhotos { get; set; }
        public IList<ProductReviewDTO> ProductReviews { get; set; }
        public IList<OrderDetailDTO> OrderDetails { get; set; }
    }
}
