﻿using eCommerce.Domain.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using eCommerce.Domain.DTO;
using eCommerce.Framework.Repository;
using AutoMapper;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class ProductPhotoService : IProductPhotoService
    {
        private readonly IProductPhotoRepository _productPhotoRepository;        

        public ProductPhotoService(IProductPhotoRepository productPhotoRepository)
        {
            this._productPhotoRepository = productPhotoRepository;            
        }

        public IEnumerable<ProductPhotoDTO> GetProductPhoto(Guid productId)
        {
            var productPhotos = _productPhotoRepository.GetMany(p=>p.ProductId==productId);            
            return Mapper.Map<List<ProductPhoto>, List<ProductPhotoDTO>>(productPhotos.ToList()); ;
        }
    }
}
