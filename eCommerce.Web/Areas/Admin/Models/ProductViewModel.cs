﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Web.Areas.Admin.Models
{
    public class ProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string UrlSlug { get; set; }
        public string RepresentImg { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
}