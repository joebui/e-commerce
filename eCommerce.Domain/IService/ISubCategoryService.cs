﻿using eCommerce.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Domain.IService
{
    public interface ISubCategoryService
    {
        IEnumerable<SubCategoryDTO> GetAll();
        IEnumerable<SubCategoryDTO> GetSubCategoriesByCategory(int categoryId);

        SubCategoryDTO GetSubCategoryById(int id);
        SubCategoryDTO GetSubCategoryBySlug(string slug);

        bool IsThisSubcategoryAvailable(string name);
        bool AreSubcategoriesOfCategoryDeleted(int categoryId);

        void Create(SubCategoryDTO subCategoryDto);
        void Update(SubCategoryDTO subCategoryDTO);
        void Delete(SubCategoryDTO subCategoryDTO);        
    }
}
