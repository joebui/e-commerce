﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;
using eCommerce.Service.Service;
using eCommerce.UnitTest.Utilities;
using Moq;
using NUnit.Framework;
using eCommerce.Domain.DTO;
using eCommerce.Ultility.Type;
using System.Linq.Expressions;
using AutoMapper;

namespace eCommerce.UnitTest.ServiceTest
{
    public class OrderServiceTests
    {
        private Mock<IOrderRepository> _repo;
        private Mock<IOrderDetailRepository> _repoDetails;
        private Mock<IProductRepository> _repoProduct;
        private IOrderService _service;
        private Mock<IUnitOfWork> _unit;
        private readonly List<Order> _orders = new List<Order>();

        [SetUp]
        public void Setup()
        {
            _repo = new Mock<IOrderRepository>();
            _repoDetails = new Mock<IOrderDetailRepository>();
            _repoProduct = new Mock<IProductRepository>();
            _unit = new Mock<IUnitOfWork>();
            _service = new OrderService(_repo.Object, _unit.Object, _repoDetails.Object, _repoProduct.Object);
            AutoMappingRegistration.Register();

            _orders.Add(new Order() { Id = Guid.NewGuid(), Quantity = 3, Total = 45, State = 0, User = new User {Email="email@mail.com" } });
            _orders.Add(new Order() { Id = Guid.NewGuid(), Quantity = 4, Total = 23, State = 0 });
            _orders.Add(new Order() { Id = Guid.NewGuid(), Quantity = 10, Total = 5, State = 0 });
            _orders.Add(new Order() { Id = Guid.NewGuid(), Quantity = 2, Total = 4, State = 0 });

        }

        [Test]
        public void GetAllOrder_ShouldReturnListMockOrders()
        {
            _repo.Setup(r => r.GetMany(It.IsAny<Expression<Func<Order, bool>>>())).Returns(_orders);

            var result = _service.GetAll() as List<OrderDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(_orders.Count, result.Count);
        }

        [Test]
        public void CancelAnPendingOrder_ShouldChangeOrderStateToCanceled()
        {
            var order = _orders.First();
            _repo.Setup(r => r.GetById(It.IsAny<Guid>())).Returns(order);

            _service.CancelOrder(order.Id);

            Assert.AreEqual(3, order.State);
        }
        [Test]
        public void ApproveAnPendingOrder_ShouldChangeOrderStateToProcessed()
        {
            var order = _orders.First();
            _repo.Setup(r => r.GetById(It.IsAny<Guid>())).Returns(order);

            var result = _service.ApproveOrder(order.Id);

            Assert.AreEqual(1,order.State);
        }

        [Test]
        public void DeleteAnOrder_ShouldMarkThatOrderAsDeleted()
        {
            var order = _orders.First();
            _repo.Setup(r => r.GetById(It.IsAny<Guid>())).Returns(order);

            _service.DeleteOrder(order.Id);

            Assert.IsTrue(order.IsDeleted);
        }

        [Test]
        public void GetOrderById_NotFound_ShouldReturnNull()
        {
            _repo.Setup(r => r.GetById(It.IsAny<Guid>())).Returns((Order)null);

            var result = _service.GetOrderById(Guid.NewGuid()) as OrderDTO;

            Assert.IsNull(result);
        }

        [Test]
        public void GetOrderById_Found_ShouldReturnOrder()
        {
            var order = _orders.First();
            _repo.Setup(r => r.GetById(order.Id)).Returns(order);

            var result = _service.GetOrderById(order.Id) as OrderDTO;

            Assert.IsNotNull(result);
            Assert.AreEqual(order.Quantity, result.Quantity);
        }

        [Test]
        public void GetOrderByUserId_Success_ShouldReturnOrder()
        {
            _repo.Setup(o => o.GetMany(It.IsAny<Expression<Func<Order, bool>>>())).Returns(_orders);

            var result = _service.GetOrderByUserId(Guid.NewGuid()) as List<OrderDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(_orders.Count,result.Count);
        }
        [Test]
        public void AddNewValidOrder_TheCommitMethodShouldRunOnce()
        {
            _service.Create(Guid.NewGuid(),10,new List<OrderDetailDTO>());

            _unit.Verify(u => u.Commit(), Times.Once);
        }


    }
}
