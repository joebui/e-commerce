﻿using System.Collections.Generic;
using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Framework.Repository;
using eCommerce.Schema.Entity;

namespace eCommerce.Service.Service
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;        

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;            
        }

        public RoleDTO GetRoleByName(string name)
        {
            var role = _roleRepository.Get(r => r.Name.Equals(name));
            return Mapper.Map<Role, RoleDTO>(role);
        }

        public RoleDTO GetRoleById(int id)
        {
            var role = _roleRepository.GetById(id);
            return Mapper.Map<Role, RoleDTO>(role);
        }

        public IEnumerable<RoleDTO> GetAllRoles()
        {
            var role = _roleRepository.GetAll();
            return Mapper.Map<IEnumerable<Role>, IEnumerable<RoleDTO>>(role);
        }
    }
}
