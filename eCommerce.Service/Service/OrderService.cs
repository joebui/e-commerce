﻿using eCommerce.Domain.IService;
using eCommerce.Framework.Infrastructure;
using eCommerce.Framework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using eCommerce.Domain.DTO;
using AutoMapper;
using eCommerce.Schema.Entity;
using eCommerce.Ultility.Type;

namespace eCommerce.Service.Service
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductRepository _productRepository;

        public OrderService(IOrderRepository orderRepository, IUnitOfWork unitOfWork, IOrderDetailRepository orderDetailRepository, IProductRepository productRepository)
        {
            _orderRepository = orderRepository;
            _unitOfWork = unitOfWork;
            _orderDetailRepository = orderDetailRepository;
            _productRepository = productRepository;
        }

        public decimal CalculateTotalPrice(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);

            return order.OrderDetails.Sum(od => od.Quantity * od.Product.Price);
        }

        public int CalculateTotalProductQuantity(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);

            return order.OrderDetails.Sum(od => od.Quantity);
        }

        public void CancelOrder(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            order.State = 3;
            _unitOfWork.Commit();
        }

        public IEnumerable<OrderDTO> GetAll()
        {
            var orders = _orderRepository.GetMany(p => !p.IsDeleted);
            var ordersDTO = Mapper.Map<List<Order>, List<OrderDTO>>(orders.ToList());

            return ordersDTO;
        }

        public IEnumerable<OrderDTO> GetOrderByUsername(string name)
        {
            var orders = _orderRepository.GetMany(o => o.User.UserName.Contains(name));
            return Mapper.Map<List<Order>, List<OrderDTO>>(orders.ToList());
        }

        public IEnumerable<OrderDTO> GetOrderByState(string state)
        {
            if (state == "Pending")
            {
                var orders = _orderRepository.GetMany(o => o.State == (int)ProductState.Pending);
                return Mapper.Map<List<Order>, List<OrderDTO>>(orders.ToList());
            }
            else
            {
                var orders = _orderRepository.GetMany(o => o.State == (int)ProductState.Approved);
                return Mapper.Map<List<Order>, List<OrderDTO>>(orders.ToList());
            }           
        }

        public OrderDTO GetOrderById(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            var orderDTO = Mapper.Map<Order, OrderDTO>(order);
            return orderDTO;
        }

        public IEnumerable<OrderDetailDTO> GetOrderDetail(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            var orderDetailsDTO = Mapper.Map<List<OrderDetail>, List<OrderDetailDTO>>(order.OrderDetails.ToList());

            return orderDetailsDTO;
        }

        public Guid Create(Guid userId, int quantity, IEnumerable<OrderDetailDTO> orderDetailsDTO)
        {
            var order = new Order
            {
                Id = Guid.NewGuid(),
                CreatedBy = userId,
                UserId = userId,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                UpdatedBy = userId,
                Quantity = quantity,
                State = 0,
                OrderDetails = new List<OrderDetail>()
            };

            var total = 0.00M;
            
            var orderDetails = Mapper.Map<List<OrderDetailDTO>, List<OrderDetail>>(orderDetailsDTO.ToList());

            foreach (var x in orderDetails)
            {
                x.OrderId = order.Id;
                total += (_productRepository.GetById(x.ProductId).Price * x.Quantity);
                CreateOrderDetail(x);
                order.OrderDetails.Add(x);
            }
            order.Total = total;
            _orderRepository.Add(order);
            _unitOfWork.Commit();
            return order.Id;
        }

        public void CreateOrderDetail(OrderDetail order)
        {
            _orderDetailRepository.Add(order);
        }
      
        public IEnumerable<OrderDTO> GetOrderByUserId(Guid userId)
        {
            var orders = _orderRepository.GetMany(p => p.UserId.Equals(userId));            
            return Mapper.Map<List<Order>, List<OrderDTO>>(orders.ToList()); ;
        }

        public string ApproveOrder(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            order.State = 1;
            _unitOfWork.Commit();
            return order.User.Email;
        }

        public int CountAvailableOrdersByUserId(Guid userId)
        {
            var result = _orderRepository.GetMany(o => o.UserId == userId).Count(o => !o.IsDeleted);        
            return result;
        }

        public void DeleteOrder(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            order.IsDeleted = true;
            _orderRepository.Update(order);
            _unitOfWork.Commit();
        }
    }
}
