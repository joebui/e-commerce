﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Schema.Entity
{
    public class Role : BaseEntity<int>
    {
        public string Name { get; set; }

        public virtual IList<User> Users { get; set; }
    }
}
