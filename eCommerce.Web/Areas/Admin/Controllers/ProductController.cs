﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Ultility.Type;
using eCommerce.Web.Controllers;
using PagedList;
using Microsoft.AspNet.Identity;

namespace eCommerce.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly ISubCategoryService _subCategoryService;

        public ProductController(IProductService productService, ICategoryService categoryService, ISubCategoryService subCategoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
        }

        // GET: Admin/Product
        public ActionResult Index(int page = 1)
        {
            ViewBag.Page = page;
            ViewBag.Search = new SelectList(new List<string>() { "All", ProductState.Approved.ToString(), ProductState.Pending.ToString() });
            var products = _productService.GetAllAdmin();
            var viewModel = Mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductModel>>(products);
            return View(viewModel.OrderByDescending(p => p.CreatedDate).ToPagedList(page,10));
        }

        // GET: Admin/Product/Detail
        public ActionResult Detail(Guid id, int page = 1)
        {
            ViewBag.Page = page;
            var productDto = _productService.GetById(id);
            var viewModel = Mapper.Map<ProductDTO, ProductDetailModel>(productDto);            
            return View(viewModel);
        }

        public ActionResult Edit(Guid id, int page = 1)
        {
            ViewBag.Page = page;
            var productDto = _productService.GetById(id);
            var viewModel = Mapper.Map<ProductDTO, Models.ProductFormViewModel>(productDto);
            viewModel.Categories = GetCategory(viewModel.CategoryId);
            viewModel.SubCategories = GetSubCategory(viewModel.CategoryId, viewModel.SubCategoryId);            
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(Models.ProductFormViewModel viewModel)
        {
            viewModel.UpdatedBy = Guid.Parse(User.Identity.GetUserId());
            _productService.UpdateProduct(Mapper.Map<Models.ProductFormViewModel, ProductDTO>(viewModel));
            return RedirectToAction("Index");
        }

        private IEnumerable<SelectListItem> GetCategory(int selected)
        {
            var categories = _categoryService.GetAllCategory();
            var list = new List<SelectListItem>();
            foreach (var c in categories)
            {
                if (c.Id == selected)
                {
                    list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name, Selected = true });
                }
                else
                {
                    list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
                }
            }

            return list;
        }

        private IEnumerable<SelectListItem> GetSubCategory(int id, int selected)
        {
            var subCategories = _subCategoryService.GetSubCategoriesByCategory(id);
            var list = new List<SelectListItem>();
            foreach (var c in subCategories)
            {
                if (c.Id == selected)
                {
                    list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name, Selected = true });
                }
                else
                {
                    list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
                }
            }

            return list;
        }

        public ActionResult GetSubCategory(int id)
        {
            var subCategories = _subCategoryService.GetSubCategoriesByCategory(id);
            var list = new List<SelectListItem>();
            foreach (var c in subCategories)
            {
                list.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult SearchByState(string search)
        {
            if (search == "All")
            {
                var products = _productService.GetAllAdmin();
                var viewModel = Mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductModel>>(products);
                return PartialView("_SearchResult", viewModel.OrderByDescending(p => p.CreatedDate));            
            }   
            else
            {
                var products = _productService.GetProductsWithStatus(search);
                return PartialView("_SearchResult", Mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductModel>>(products));
            }         
        }

        [HttpPost]
        public PartialViewResult SearchByName(string result)
        {            
            var products = _productService.GetProductByName(result);
            return PartialView("_SearchResult", Mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductModel>>(products));         
        }

        #region ProductFunctions
        public void Delete(Guid productId)
        {
            _productService.Delete(productId, Guid.Parse(User.Identity.GetUserId()));
            _productService.Save();            
        }

        public void Approve(Guid productId)
        {
            _productService.Approve(productId, Guid.Parse(User.Identity.GetUserId()));
            _productService.Save();
        }
        #endregion
    }
}