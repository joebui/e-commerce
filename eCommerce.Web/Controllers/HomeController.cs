﻿using AutoMapper;
using eCommerce.Domain.DTO;
using eCommerce.Domain.IService;
using eCommerce.Web.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;

        public HomeController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        public ActionResult Index(string sortOrder, int page = 1, int pagesize = 12)
        {
            sortOrder = string.IsNullOrEmpty(sortOrder) ? "price_desc" : sortOrder;
            ViewBag.SortOrder = sortOrder;
            ViewBag.Pagesize = pagesize;

            var productsDto = _productService.GetAllProductsPublished();

            switch (sortOrder)
            {
                case "arrival_asc":
                    productsDto = productsDto.OrderBy(p => p.CreatedDate);
                    break;
                case "arrival_desc":
                    productsDto = productsDto.OrderByDescending(p => p.CreatedDate);
                    break;
                case "name_asc":
                    productsDto = productsDto.OrderBy(p => p.Name);
                    break;
                case "name_desc":
                    productsDto = productsDto.OrderByDescending(p => p.Name);
                    break;
                case "price_asc":
                    productsDto = productsDto.OrderBy(p => p.Price);
                    break;
                default:
                    productsDto = productsDto.OrderByDescending(p => p.Price);
                    break;
            }

            var productVm = new ProductViewModel();

            productVm.Products = Mapper.Map<List<ProductDTO>, List<ProductModel>>(productsDto.ToList()).ToPagedList(page, pagesize);
            productVm.Categories = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(_categoryService.GetAllCategory().ToList());
            productVm.Recommended = Mapper.Map<List<ProductDTO>, List<ProductModel>>(_productService.GetRecommendedProduct().ToList()).ToList();

            return View(productVm);
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Search(string key, string sortOrder, int page = 1, int pageSize = 12)
        {
            sortOrder = string.IsNullOrEmpty(sortOrder) ? "price_desc" : sortOrder;
            ViewBag.SortOrder = sortOrder;
            ViewBag.Key = key;
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;

            var productsDto = _productService.GetProductByName(key);

            switch (sortOrder)
            {
                case "publishedDate_asc":
                    productsDto = productsDto.OrderBy(p => p.UpdatedDate);
                    break;
                case "publishedDate_desc":
                    productsDto = productsDto.OrderBy(p => p.UpdatedDate);
                    break;
                case "name_asc":
                    productsDto = productsDto.OrderBy(p => p.Name);
                    break;
                case "name_desc":
                    productsDto = productsDto.OrderByDescending(p => p.Name);
                    break;
                case "price_asc":
                    productsDto = productsDto.OrderBy(p => p.Price);
                    break;
                default:
                    productsDto = productsDto.OrderByDescending(p => p.Price);
                    break;
            }

            var productVm = new ProductViewModel();

            productVm.Products = Mapper.Map<List<ProductDTO>, List<ProductModel>>(productsDto.ToList()).ToPagedList(page, pageSize);
            productVm.Categories = Mapper.Map<List<CategoryDTO>, List<CategoryModel>>(_categoryService.GetAllCategory().ToList());

            return View(productVm);
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}