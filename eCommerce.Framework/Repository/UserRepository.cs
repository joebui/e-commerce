﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Framework.Infrastructure;
using eCommerce.Schema.Entity;

namespace eCommerce.Framework.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        
    }

    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
